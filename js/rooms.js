 $(document).ready(function(){
       $("#new-room").click(function(){
          $("<tr class='editable new-row' ><input type='hidden' name='newAvailability[]' class='hiddenF' value='0'></input><td><input type='text' name='newRoomName[]' class='table-input name' placeholder='Room name' ></input></td><td><input type='number' name='newCapacity[]' min='0' class='table-num capacity' value='0' ></input></td><td>R <input type='number' name='newPricePH[]' min='0' class='table-num pricePH' value='0' ></input></td><td>R <input type='number' name='newPricePD[]' min='0' class='table-num pricePD' value='0' ></input></td><td class='availability clickable-lbl available'>available</td><td><input type='number' name='newMinHireTime[]' min='0' class='table-num minHireTime' value='0' ></input></td><td><input type='time' step='1' name='newAvailableFrom[]' class='table-input availableFrom' ></input></td><td><input type='time' step='1' name='newAvailableTo[]' class='in-time table-input availableTo' ></td><td><input type='file' name='newImageName[]' class='table-input image' placeholder='Room image'></input></td></tr>").insertBefore('#new-room')
       });
       
       $("table").on('click', '.clickable-lbl', function(e){
           $(this).toggleClass("available");
           if($(this).text() == "available"){
               $(this).text("unavailable");
               $(".hiddenF", $(this).parent()).val('1');
           } else {
               $(this).text("available");
               $(".hiddenF", $(this).parent()).val('0');
           }
       });
     
     $(".updated").click(function(){
         $(this).removeClass("updated");
         $(this).addClass("outdated")
     });
     
     $("#form-rooms").submit(function(e){
         $("tr").each(function(){
             var dStart = Date.parse('01/01/2011 ' + $(".availableFrom", this).val());
             var dEnd = Date.parse('01/01/2011 ' + $(".availableTo", this).val());
             if(dStart >= dEnd){
                 alert("Available from can not be later than available to.");
                 e.preventDefault();
             }
         });
     });
   });
