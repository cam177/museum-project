$(function(){

    var getEvents = [];
    var curEvent;
    function load_events(){
     $.each(events, function(index, item){
        var myEvent ={};
        var startDate = new Date(item["startDate"] + "T" + item["startTime"]);
        startDate.setHours(startDate.getHours() - 2);
        var endDate = new Date(item["endDate"] + "T" + item["endTime"]);
        endDate.setHours(endDate.getHours() - 2);
        var bck
        if (item["approved"] == 0){
         bck = 'green';
     } else if (item["approved"] == 1){
        bck = 'red';
    } else {
        bck = 'blue';
    }

    myEvent = {
        ID: item["ID"],
        email: item["email"],
        start: startDate,
        end: endDate,
        name: item["name"] + " " + item["surname"],
        room: item["room"],
        sd: item["startDate"],
        ed: item["endDate"],
        st: item["startTime"],
        et: item["endTime"],
        attendees: item["attendees"],
        eventType: item["eventType"],
        cost: item["cost"],
        hours: item["totalHours"],
        approved: item['approved'],
        uploadedChecklist: item["uploadedChecklist"],
        color: bck,
        title: item["name"] + " " + item["surname"] + ": " + item["room"]
    };
    getEvents.push(myEvent);

});
 }


 load_events();

 $(document).ready(function() {
     var dates = {
        convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
            );
    },
    compare:function(a,b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
            );
    },
    inRange:function(d,start,end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(d=this.convert(d).valueOf()) &&
            isFinite(start=this.convert(start).valueOf()) &&
            isFinite(end=this.convert(end).valueOf()) ?
            start <= d && d <= end :
            NaN
            );
    }
}


$('#calendar').fullCalendar({
 header: {
    left: 'prev,next today',
    center: 'title',
    right: 'month,basicWeek,basicDay'
},
defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: getEvents,
            eventClick: function(calEvent, jsEvent, view) {
                if(calEvent.approved == '2'){
                    $("#approve-btn").removeClass("disabled");
                    $("#reject-btn").removeClass("disabled");

                    $("#upload-but").addClass("disabled");   
                    $("#upload-but").attr("disabled",true);         
                } else {
                    $("#approve-btn").addClass("disabled");
                    $("#reject-btn").addClass("disabled");
                    console.log(calEvent.uploadedChecklist);
                    if(dates.compare(calEvent.sd, new Date()) != -1  && calEvent.uploadedChecklist == null) {
                        $("#upload-but").addClass("disabled");
                        $("#upload-but").attr("disabled",true);
                    } else {
                        $("#upload-but").removeClass("disabled");
                        $("#upload-but").attr("disabled",false);
                        
                    }

                }
                curEvent = calEvent.ID;
                $(".email-input").val(calEvent.email);
                $(".name-input").val(calEvent.name);
                $(".room-input").val(calEvent.room);
                $(".sd-input").val(calEvent.sd);
                $(".ed-input").val(calEvent.ed);
                $(".st-input").val(calEvent.st);
                $(".et-input").val(calEvent.et);
                $(".attendees-input").val(calEvent.attendees);
                $(".event-input").val(calEvent.eventType);
                $(".cost-input").val(calEvent.cost);
                $(".hours-input").val(calEvent.hours);
                event.preventDefault(); 
                history.pushState({},null,"manage_bookings.php?bID="+curEvent);

            }
        });
});


$("#approve-btn").click(function(){
    if(!$(this).hasClass('disabled')){
        var ajaxurl = 'manage_bookings.php';
        var data = {action: 'approved', event: curEvent};
        $.post(ajaxurl, data, function(){
            load_events();
            $('#calendar').fullCalendar('refetchEvents');
            $("#approve-btn").addClass("disabled");
            $("#reject-btn").addClass("disabled");
            window.location.reload();
        })
    }
});

$("#reject-btn").click(function(){
    if(!$(this).hasClass('disabled')){
        var ajaxurl = 'manage_bookings.php';
        var data = {action: 'rejected', event: curEvent};
        $.post(ajaxurl, data, function(){
            load_events();
            $('#calendar').fullCalendar('refetchEvents');
            $("#approve-btn").addClass("disabled");
            $("#reject-btn").addClass("disabled");
            window.location.reload();
        })
    }
});
});