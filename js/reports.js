$(function () {

    var chart;
    $(document).ready(function() {
        $.getJSON("./../php/graph_users.php", function(json) {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo:container,

                    type: 'column'
                },
                title: {
                    text: 'Number of registered Users'
                },
                xAxis: {
                    categories: [
                    'All Time'
                    ]
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount'
                    }, 
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} User/s</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: json
            });

        });
        $.getJSON("./../php/graph_bookings.php", function(json) {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo:container2,
                    type: 'column'
                },
                title: {
                    text: 'Number of Bookings per User'
                },
                xAxis: {
                    type: "category"
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Amount'
                    }, 
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} Bookings</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: json,
            });
            chart.xAxis[0].setCategories(chart.series[0].userOptions.Name);
            for(var i=0;i<chart.series[0].xData.length;i++) {

                chart.series[0].data[i].color = getRandomColor();
            }
        });

    });
    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
});