$(document).ready(function(){

      start_time();
   end_time();
   
   var D = new Date();
   var dates = {
    convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
            );
    },
    compare:function(a,b) {
        return (
            isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
            );
    }    
}
if(dates.compare(D,startD) >= 7) {
    document.getElementById("cancel_btn").style.visibility = 'visible';
} else {
    document.getElementById("cancel_btn").style.visibility = 'hidden';
}

});

function start_time() {
   $(".start_time").keyup( function() {
       var val = $(this).val();
       if (val.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/i)){
           $(".alert_start_time").hide();
           document.getElementById("save_btn").disabled = false; 
       } else {
           $(".alert_start_time").text("Format: HH:MM:SS");
           $(".alert_start_time").show();
           document.getElementById("save_btn").disabled = true; 
       }
   });
}

function end_time() {
   $(".end_time").keyup( function() {
       var val = $(this).val();
       
       if (val.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/i)){
           $(".alert_end_time").hide();
           document.getElementById("save_btn").disabled = false;
       } else {
           $(".alert_end_time").text("Format: HH:MM:SS");
           $(".alert_end_time").show();
           document.getElementById("save_btn").disabled = true;
       }
   });
}
