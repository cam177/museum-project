$(function() {
	$("#form-signup").on("submit", function (e) {
		$(".notification").remove();
		var firstname = $("#firstname-signup").val();
		firstname = firstname.match(/^[a-z]{1}([a-z]{1,}|-{1}[a-z]{1}){1,}$/i);
		var lastname = $("#lastname-signup").val();
		lastname = lastname.match(/^[a-z]{1}([a-z]{1,}|-{1}[a-z]{1}){1,}$/i);
		var email = $("#email-signup").val();
		email = email.match(/^[a-z0-9]{1,}@[a-z0-9]{1,}(\.[a-z0-9]{2,}){1,}$/i);
		var pass = $("#pass-signup").val();
		pass = pass.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/i);
		var passconf = $("#passconf-signup").val();
		passconf = passconf.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/i);
		var msg = "Please enter valid ";
		if (!firstname || firstname.length <= 0) {
			msg += "firstname, ";
			$("#firstname-signup-group").addClass("has-error");
		}
		if (!lastname || lastname.length <= 0) {
			msg += "lastname, ";
			$("#lastname-signup-group").addClass("has-error");
		}
		if (!email || email.length <= 0) {
			msg += "email address, ";
			$("#email-signup-group").addClass("has-error");
		}
		if (!pass || pass.length <= 0) {
			msg += "password, ";
			$("#pass-signup-group").addClass("has-error");
		}
		if (!passconf || passconf.length <= 0) {
			msg += "password confirmation, ";
			$("#passconf-signup-group").addClass("has-error");
		}
		if (msg != "Please enter valid ") {
			msg = msg.substring(0, msg.length - 2);
			var and = msg.lastIndexOf(",");
			if (and !== -1) {
				msg = msg.substring(0, and + 1) + " and " + msg.substring(and + 1);
			}
			$('<div class="notification red">' + msg + '</div>').prependTo("#modal-signup .modal-body");
            e.preventDefault();
            return false;
		}
	});
	$("#form-login").on("submit", function (e) {
		$(".notification").remove();
		var email = $("#email-login").val();
		email = email.match(/^[a-z0-9]{1,}@[a-z0-9]{1,}(\.[a-z0-9]{2,}){1,}$/i);
		var pass = $("#pass-login").val();
		pass = pass.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/i);
		var msg = "Please enter valid ";
		if (!email || email.length <= 0) {
			msg += "email address, ";
			$("#email-login-group").addClass("has-error");
		}
		if (!pass || pass.length <= 0) {
			msg += "password, ";
			$("#pass-login-group").addClass("has-error");
		}
		if (msg != "Please enter valid ") {
			msg = msg.substring(0, msg.length - 2);
			var and = msg.lastIndexOf(",");
			if (and !== -1) {
				msg = msg.substring(0, and + 1) + " and " + msg.substring(and + 1);
			}
			$('<div class="notification red">' + msg + '</div>').prependTo("#modal-login .modal-body");
            e.preventDefault();
            return false;
		}
	});
	$("#pass-login").bind("cut copy paste",function(e) {
			e.preventDefault();
	});
	$("#pass-signup").bind("cut copy paste",function(e) {
			e.preventDefault();
	});
	$("#passconf-signup").bind("cut copy paste",function(e) {
			e.preventDefault();
	});
	$("#pass-view-signup").mousedown(function() {
		$("#pass-signup").attr("type", "text");
	});
	$("#pass-view-signup").mouseup(function() {
		$("#pass-signup").attr("type", "password");
	});
	$("#passconf-view-signup").mousedown(function() {
		$("#passconf-signup").attr("type", "text");
	});
	$("#passconf-view-signup").mouseup(function() {
		$("#passconf-signup").attr("type", "password");
	});
	$("#pass-view-login").mousedown(function() {
		$("#pass-login").attr("type", "text");
	});
	$("#pass-view-login").mouseup(function() {
		$("#pass-login").attr("type", "password");
	});
});