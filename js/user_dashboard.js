$(document).ready(function() {
        var getEvents = [];
        var curEvent;
        var bookingID;
        function load_events(){
        $.each(events, function(index, item){
            var myEvent ={};
            var startDate = new Date(item["startDate"] + "T" + item["startTime"]);
            startDate.setHours(startDate.getHours() - 2);
            var endDate = new Date(item["endDate"] + "T" + item["endTime"]);
            endDate.setHours(endDate.getHours() - 2);
            var bck
            if (item["userID"] == userIDs){
               bck = 'green';
           } else {
               bck = 'red';
           }
           myEvent = {
            ID: item["ID"],
            userID: item["userID"],
            email: item["email"],
            start: startDate,
            end: endDate,
            name: item["name"] + " " + item["surname"],
            room: item["roomID"],
            sd: item["startDate"],
            ed: item["endDate"],
            st: item["startTime"],
            et: item["endTime"],
            attendees: item["attendees"],
            eventType: item["eventType"],
            cost: item["cost"],
            hours: item["totalHours"],
            approved: item['approved'],
            color: bck,
            title: item["roomName"]
        };
        getEvents.push(myEvent);
    });
    }
    load_events();
      $('#calendar').fullCalendar({
       header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: getEvents,
            eventClick: function(calEvent, jsEvent, view) {
                if (calEvent.userID == userIDs) {
                    window.location.href = "view_booking.php?bID="+calEvent.ID;
                }
            }
        });
  });
function run(ID){
            window.location.href = "view_booking.php?bID="+ID;
      }



