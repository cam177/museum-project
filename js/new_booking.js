valid = 1;

$(document).ready( function() {
    j = 1;
    $("#plus").click(function(){
        $("#mainRoom").clone().insertAfter("div.room_panel:last");
        //start_time(j);
        //end_time(j);
        //attendees(j);
        //appendRoomID(j);
        rooms(j);
        j++;
    });
    start_time(0);
    end_time(0);
    attendees(0);
    rooms(0);
    start_date(0);
    end_date(0);
    totalTime();
    totalCost();
    chosenRooms();
    preventSubmit();
    backFoyer();
    
    $('.submit').click(function(){
        $('.attendees').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_attendees").eq(key).text("Should not be empty");
                $(".alert_attendees").eq(key).show();
                valid = 1;
            }
        });
        $('.start_time').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_start_time").eq(key).text("Should not be empty");
                $(".alert_start_time").eq(key).show();
            }
        });
        $('.end_time').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_end_time").eq(key).text("Should not be empty");
                $(".alert_end_time").eq(key).show();
            }
        });
        $('.start_date').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_start_date").eq(key).text("Should not be empty");
                $(".alert_start_date").eq(key).show();
            } else {
                $(".alert_start_date").eq(key).hide();
            }
        });
        $('.end_date').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_end_date").eq(key).text("Should not be empty");
                $(".alert_end_date").eq(key).show();
            } else {
                $(".alert_end_date").eq(key).hide();
            }
        });
        $('.rooms').each(function(key, value) {
            if ( $(this).val() === '' ) {
                $(".alert_rooms").eq(key).text("A room must be chosen");
                $(".alert_rooms").eq(key).show();
            } else {
                $(".alert_rooms").eq(key).hide();
            }
        });
        
        if (valid === 0) {
            $(".rooms").hide();
            
        } else {
            $(".alert_submit").show();
            $(".alert_submit").text("Please fix errors above before submitting");
        }
    });
    
});

function rooms(roomCount) {
        $('.rooms').eq(roomCount).find('.room').each(function(key, value) {
            $(this).click(function(e) {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        values = this.responseText.split("/");
                        $('.roomName').eq(roomCount).val(values[0]);
                        $('.capacity').eq(roomCount).val(values[1]);
                        $('.tariff_per_hour').eq(roomCount).val(values[2]);
                        $('.tariff_per_day').eq(roomCount).val(values[3]);
                        $('.minimum_hire_time').eq(roomCount).val(values[4]);
                        $('.roomID').eq(roomCount).val(values[5]);
                        $('.avail_from').eq(roomCount).val(values[6]);
                        $('.avail_to').eq(roomCount).val(values[7]);
                    }
                };
                xmlhttp.open("GET", "./../php/room_info.php?q=" + key, true);
                xmlhttp.send();
                e.preventDefault();
            });
        });
    //});
}

function attendees(roomCount) {
    $(".attendees").eq(roomCount).keyup( function() {
        var val = parseInt($(this).val(), 10);
        if (val < 0) {
            $(".alert_attendees").eq(roomCount).text("Must be larger than 0");
            $(".alert_attendees").eq(roomCount).show();
            valid = 1;
        } else if (val % 1 != 0) {
            $(".alert_attendees").eq(roomCount).text("Must be a whole number");
            $(".alert_attendees").eq(roomCount).show();
            valid = 1;
        } else {
            $(".alert_attendees").eq(roomCount).hide();
            valid = 0;
        }
    });
}

function start_time(roomCount) {
    $(".start_time").eq(roomCount).keyup( function() {
        var val = $(this).val();
        if (val.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/i)){
            $(".alert_start_time").eq(roomCount).hide();
            valid = 0;
        } else {
            $(".alert_start_time").eq(roomCount).text("Needs to be in format HH:MM");
            $(".alert_start_time").eq(roomCount).show();
            valid = 1;
        }
    });
}

function end_time(roomCount) {
    $(".end_time").eq(roomCount).keyup( function() {
        var val = $(this).val();
        if (val.match(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/i)){
            $(".alert_end_time").eq(roomCount).hide();
            valid = 0;
        } else {
            $(".alert_end_time").eq(roomCount).text("Needs to be in format HH:MM");
            $(".alert_end_time").eq(roomCount).show();
            valid = 1;
        }
    });
}

function start_date(roomCount) {
    $(".start_date").eq(roomCount).keyup( function() {
        var val = $(this).val();
        if (val.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/i)){
            $(".alert_start_date").eq(roomCount).hide();
            valid = 0;
        } else {
            $(".alert_start_date").eq(roomCount).text("Needs to be in format YYYY-MM-DD");
            valid = 1;
            $('.valid').val(valid);
        }
    });
}

function end_date(roomCount) {
    $(".end_date").eq(roomCount).keyup( function() {
        var val = $(this).val();
        if (val.match(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/i)){
            $(".alert_end_date").eq(roomCount).hide();
            valid = 0;
        } else {
            $(".alert_end_date").eq(roomCount).text("Needs to be in format YYYY-MM-DD");
            valid = 1;
            $('.valid').val(valid);
        }
    });
}

function totalTime() {
    window.setInterval(function () {
        totalHours = 0;
        $('.start_time').each(function(key, value) {
            t1 = $('.start_time').eq(key).val().split(":");
            if (Number(t1[0])) {
                /*if ((t1[0] > 11)&&(t1[0] < 21)) {
                    
                } else {
                    $(".alert_start_time").eq(key).text("Rooms are only available from 11:00 to 21:00");
                    $(".alert_start_time").eq(key).show();
                    valid = 1;
                }*/
                $af_hr = $('.avail_from').eq(key).val().split(":");
                $at_hr = $('.avail_to').eq(key).val().split(":");
                if ((t1[0] >= $af_hr[0])&&(t1[0] <= $at_hr[0])) {
                    
                } else {
                    $(".alert_start_time").eq(key).text("Rooms are only available from "+$('.avail_from').eq(key).val()+" to "+$('.avail_to').eq(key).val());
                    $(".alert_start_time").eq(key).show();
                    valid = 1;
                }
            }
            
            tt1 = new Date(0, 0, 0, t1[0], t1[1], 0, 0);
            t2 = $('.end_time').eq(key).val().split(":");
            if (Number(t2[0])) {
                $af_hr = $('.avail_from').eq(key).val().split(":");
                $at_hr = $('.avail_to').eq(key).val().split(":");
                if ((t2[0] >= $af_hr[0])&&(t2[0] <= $at_hr[0])) {
                    
                } else {
                    $(".alert_end_time").eq(key).text("Rooms are only available from "+$('.avail_from').eq(key).val()+" to "+$('.avail_to').eq(key).val());
                    $(".alert_end_time").eq(key).show();
                    valid = 1;
                }
                /*if ((t2[0] > 11)&&(t2[0] < 21)) {
                    
                } else {
                    $(".alert_end_time").eq(key).text("Rooms are only available from 11:00 to 21:00");
                    $(".alert_end_time").eq(key).show();
                    valid = 1;
                }*/
            }
            
            tt2 = new Date(0, 0, 0, t2[0], t2[1], 0, 0);
            diff = tt2.getTime() - tt1.getTime();
            diffHours = diff / (100000*36);
            if (Number(diffHours)) {
                if (diffHours < 0) {
                    $(".alert_start_time").eq(key).text("Start Time must be before End Time");
                    $(".alert_start_time").eq(key).show();
                    $(".alert_end_time").eq(key).text("End Time must be after Start Time");
                    $(".alert_end_time").eq(key).show();
                    valid = 1;
                } else {
                    //$(".alert_start_time").eq(key).hide();
                    //$(".alert_end_time").eq(key).hide();
                    valid = 0;
                }
            }
            
            d1 = $('.start_date').eq(key).val().split("-");
            d2 = $('.end_date').eq(key).val().split("-");
            dd1 = new Date(d1[0], d1[1], d1[2], 0, 0, 0, 0);
            dd2 = new Date(d2[0], d2[1], d2[2], 0, 0, 0, 0);
            diff2 = dd2.getTime() - dd1.getTime(); 
            diffDays = diff2 / (1000*60*60*24); 
            if (Number(diffDays)) {
                if (diffDays < 0) {
                    $(".alert_start_date").eq(key).text("Start Date must be before End Date");
                    $(".alert_start_date").eq(key).show();
                    $(".alert_end_date").eq(key).text("End Date must be after Start Date");
                    $(".alert_end_date").eq(key).show();
                    valid = 1;
                } else {
                    //$(".alert_start_date").eq(key).hide();
                    //$(".alert_end_date").eq(key).hide();
                    valid = 0;
                }
            }

            diffTotal = (diffDays + 1) * diffHours;
            if (Number(diffTotal)) {
                totalHours = totalHours + diffTotal;
            }
        });
        $('.total_hours').val(totalHours);
        $('.total_hoursH').val(totalHours);
        
    }, 1000);
}

function totalCost() {
    window.setInterval(function () {
        totalHours = 0;
        $('.tariff_per_hour').each(function(key, value) {
            th = $('.tariff_per_hour').eq(key).val();
            td = $('.tariff_per_day').eq(key).val();
            af = $('.avail_from').eq(key).val();
            at = $('.avail_to').eq(key).val();
            
            t1 = $('.start_time').eq(key).val().split(":");
            tt1 = new Date(0, 0, 0, t1[0], t1[1], 0, 0);
            t2 = $('.end_time').eq(key).val().split(":");
            tt2 = new Date(0, 0, 0, t2[0], t2[1], 0, 0);
            diff = tt2.getTime() - tt1.getTime();
            diffHours = diff / (100000*36);
            
            d1 = $('.start_date').eq(key).val().split("-");
            d2 = $('.end_date').eq(key).val().split("-");
            dd1 = new Date(d1[0], d1[1], d1[2], 0, 0, 0, 0);
            dd2 = new Date(d2[0], d2[1], d2[2], 0, 0, 0, 0);
            diff2 = dd2.getTime() - dd1.getTime(); 
            diffDays = diff2 / (1000*60*60*24); 
            
            tariffHour = 0;//diffHours*th;
            if (diffDays == 0) {
                tariffHour = diffHours*th;
            }
            tariffDay = diffDays*td;
            
            diffTotal = tariffHour + tariffDay;
            if (Number(diffTotal)) {
                totalHours = totalHours + diffTotal;
            }
        });
        $('.total_cost').val(totalHours);
        $('.total_costH').val(totalHours);
        
    }, 1000);
}

function chosenRooms() {
    window.setInterval(function () {
            roomIDs = "";
            $('.room_panel').each(function(key, value) {
                roomIDs = roomIDs + $('.roomID').eq(key).val() + "/";
            });
            $('.roomIDs').val(roomIDs);
    }, 1000);
}

function preventSubmit() {
    $("form").submit(function(e){
        if (valid == 1) {
            e.preventDefault(e);
        } else {
            
        }
    });
}

function backFoyer() {
    window.setInterval(function () {
        $('.roomID').each(function(key, value) {
                if ( $(this).val() == 5 ) {
                    t1 = $('.start_time').eq(0).val().split(":");
                    tt1 = new Date(0, 0, 0, t1[0], t1[1], 0, 0);
                    t2 = $('.end_time').eq(0).val().split(":");
                    tt2 = new Date(0, 0, 0, t2[0], t2[1], 0, 0);
                    diff = tt2.getTime() - tt1.getTime();
                    diffHours = diff / (100000*36);
                    if (($('.roomID').length < 2)||(diffHours < 2)) {
                        $(".alert_submit").show();
                        $(".alert_submit").text(" The Back Foyer can only be hired for a minimum of 2 hours, and can only be hired together with some other room");
                        valid = 1;
                    } else {
                        $(".alert_submit").hide();
                    }
                }
            });
    }, 1000);
}
