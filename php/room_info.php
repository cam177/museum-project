<?php
    require_once("./../php/config.php");

    try {
        $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
        $q = isset($_REQUEST["q"]) ? $_REQUEST["q"] : -1;
        //if (empty($q)) $q = 0;
        
        if ($q != -1) {
            $sql = "SELECT `roomName`, `capacity`, `tariffPerHour`, `tariffPerDay`, `minimumHireTime`, `ID`, availableFrom, availableTo FROM `Rooms` WHERE `ID` = ".($q+1);
            $prep = $pdo->prepare($sql);
            $prep->execute();
            $row = $prep->fetch(PDO::FETCH_ASSOC);
            echo $row['roomName'].'/'.$row['capacity'].'/'.$row['tariffPerHour'].'/'.$row['tariffPerDay'].'/'.$row['minimumHireTime'].'/'.$row['ID'].'/'.$row['availableFrom'].'/'.$row['availableTo'].'/';
        }
    } catch (PDOException $e) {
            die($e->getMessage());
    }

?>