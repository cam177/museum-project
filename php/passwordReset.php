<?php   

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return md5($randomString);
	}

	$email = $_REQUEST['email'];
	
	$link = mysqli_connect('localhost', 'root', '');
	if (!$link) {
		$output = 'Unable to connect to the database server.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_set_charset($link, 'utf8')){
		$output = 'Unable to set database connection encoding.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_select_db($link, 'Museum')){
		$output = 'Unable to locate the database.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	$result = mysqli_query($link, "SELECT * FROM Login WHERE Email = '" . $email ."'");
	$i = 0;
	$rownum = mysqli_num_rows($result);
	if ($rownum != 1) {
		$outputReset = 'Email not found. Please Register';
		include '../html/registration.php';
		exit();	
	} else {
		$emails_file = fopen("../emailsLog/emailsFile.mail", "a") or die("Unable to open file!");
		$randomString = generateRandomString();

		$body = "This is a password reset message, as requested by you. If you did not request this, please ignore it.\nTo reset your password, please go to the following link:\nhttp://localhost/rw344-project-group07/html/reset_password.php?token=".$randomString;

		$res = mysqli_query($link, "SELECT Name, Surname FROM UserDetailsTable WHERE Email = '" . $email ."'");
		$k = 0;
		$rowN = mysqli_num_rows($res);
		while ($k < $rowN){
			mysqli_data_seek($res, $k);
			$ROW = mysqli_fetch_row($res);
			$userName = $ROW[0];
			$userSurname = $ROW[1];
			$k++;
		}

		while ($i < $rownum){
			mysqli_data_seek($result, $i);
			$row = mysqli_fetch_row($result);
			$uID = $row[0];
			$i++;
		}
		$txt = "\n" . $userName . " " . $userSurname . " " . "user ID " . $uID . "\nThis is a password reset message, as requested by you. If you did not request this, please ignore it.\nTo reset your password, please go to the following link:\nhttp://localhost/rw344-project-group07/html/reset_password.php?token=".$randomString;
		fwrite($emails_file, $txt);
		fclose($emails_file);

		require_once ("./../swiftmailer-5.x/lib/swift_required.php");
		try {
			$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
	  			->setUsername('stellenboschuniversitymuseum@gmail.com')
	  			->setPassword('triggermeplenty');

			$mailer = Swift_Mailer::newInstance($transport);
			
			$message = Swift_Message::newInstance('Password Reset Token')
			  ->setFrom(array('stellenboschuniversitymuseum@gmail.com' => 'Stellenbosch University Museum'))
			  ->setTo(array($email => $userName . " " . $userSurname))
			  ->setSubject("Password Reset")
			  ->setBody($body);
		
			$result = $mailer->send($message);
		} catch (Exception $e) {
			
		}

		$result1 = mysqli_query($link, "SELECT * FROM ForgottenPasswordRequests WHERE userID = '" . $uID ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		if ($rownum == 1) {
			header("Location:  ../html/login.php?a=0");
		} else {
			$sql = "INSERT INTO ForgottenPasswordRequests SET userID = '$uID', resetToken = '$randomString'";
		    if(!mysqli_query($link, $sql)) {
		        $output = 'Error performing update: ' . mysqli_error ($link);
		        include '../html/ConnectionFail.htm';
		        exit();
		    }
		    header("Location:  ../html/login.php?a=1");							
		}
	}
?>