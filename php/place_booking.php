<?php
    require_once("./../php/config.php");

    if(session_id() == '' || !isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION['userid']) ) {
        header("Location: ../html/login.php");
    }
    if (!isset($_SESSION['userRole']) ) {
        header("Location: ../html/index.php");
    } else {
        if (!($_SESSION['userRole'] == "User")) {
            header("Location: ../html/index.php");
        }
    }

    if(isset($_POST['submit']))
    {
        try {
            $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
            
            if (checkBookedRooms() == 1) {
                $_SESSION['msg'] = "danger/Booking unsuccessful: Room already booked between these dates";
                header("Location: ./../html/user_dashboard.php");
            } else {
            
                $roomIDs = (explode("/",$_POST['roomIDs']));
                print_r ($roomIDs);
                foreach ($roomIDs as &$val) {
                    $sql = 'INSERT INTO `Bookings`(`userID`, `roomID`, `startDate`, `endDate`, `startTime`, `endTime`, `attendees`, `eventType`, `cost`, `totalHours`, `approved`) VALUES ('.$_SESSION['userid'].', '.$val.', "'.$_POST['avaliableFrom'].'", "'.$_POST['avaliableTo'].'", "'.$_POST['startTime'].'", "'.$_POST['endTime'].'", '.$_POST['attendees'].', "'.$_POST['type'].'", '.$_POST['total_cost'].', '.$_POST['total_hours'].', 0)';
                    $prep = $pdo->prepare($sql);
                    $prep->execute();
                }
                $_SESSION['msg'] = "success/Booking Successful";
                header("Location: ./../html/user_dashboard.php");
                require_once ("./../swiftmailer-5.x/lib/swift_required.php");

                // ------------------------------------------------------------------------------------ //

                $body = "There are new bookings on the Museum system. Please head over and approve them.";

                try {
                    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
                        ->setUsername('stellenboschuniversitymuseum@gmail.com')
                        ->setPassword('triggermeplenty');

                    $mailer = Swift_Mailer::newInstance($transport);

                    $message = Swift_Message::newInstance('New Booking Notification')
                      ->setFrom(array('stellenboschuniversitymuseum@gmail.com' => 'Stellenbosch University Museum'))
                      ->setTo(array(ADMIN_EMAIL))
                      ->setSubject("New Booking")
                      ->setBody($body);

                    $result = $mailer->send($message);
                } catch (Exception $e) {

                }
                
            }

        } catch (PDOException $e) {
            die($e->getMessage());
        } 
    }

    function checkBookedRooms() {
        $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
        $sql = "SELECT ID FROM Bookings WHERE roomID = ".$_SESSION['userid']." AND '".$_POST['avaliableFrom']."' between startDate and endDate OR '".$_POST['avaliableTo']."' between startDate and endDate";
        $prep = $pdo->prepare($sql);
        $prep->execute();
        $check = 0;
        while ($row = $prep->fetch(PDO::FETCH_ASSOC)) {
            return 1;
        }
        return 0;
    }

?>