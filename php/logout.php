<?php
    session_start();
    unset($_SESSION['password']);
    unset($_SESSION['userid']);
    unset($_SESSION['userRole']);
    header("Location: ../html/");
?>