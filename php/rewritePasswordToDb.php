<?php   
	if (isset($_REQUEST['email']) && isset($_REQUEST['new_password']) && isset($_REQUEST['new_password_confirm']) && isset($_REQUEST['token']))  {
		if ($_REQUEST['new_password'] != $_REQUEST['new_password_confirm']) {
			header("Location: ../html/reset_password.php?token=".$_REQUEST['token']."&pw=no");
		}
		$email = $_REQUEST['email'];
		$new_password = $_REQUEST['new_password']; 
		$token = $_REQUEST['token'];

		$link = mysqli_connect('localhost', 'root', '');
		if (!$link) {
			$output = 'Unable to connect to the database server.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		if (!mysqli_set_charset($link, 'utf8')){
			$output = 'Unable to set database connection encoding.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		if (!mysqli_select_db($link, 'Museum')){
			$output = 'Unable to locate the database.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		$result = mysqli_query($link, "SELECT userID FROM ForgottenPasswordRequests WHERE resetToken = '" . $token ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		if ($rownum == 1) {
			while ($i < $rownum){
				mysqli_data_seek($result, $i);
				$row = mysqli_fetch_row($result);
				$userID = $row[0];
				$i++;
			}

			$result = mysqli_query($link, "SELECT ID FROM UserDetailsTable WHERE Email = '" . $email ."'");
			$i = 0;
			$rownum = mysqli_num_rows($result);
			while ($i < $rownum){
				mysqli_data_seek($result, $i);
				$row = mysqli_fetch_row($result);
				$userIDFromUserTable = $row[0];
				$i++;
			}

			if ($userIDFromUserTable != $userID) {
				$output = 'Error performing update: ' . mysqli_error ($link);
		        include '../html/ConnectionFail.htm';
		        exit();
			}

			$hash_new = md5($new_password);

			$sql = "UPDATE Login SET Password = '$hash_new' WHERE userID = '$userID'";
		    if(!mysqli_query($link, $sql)) {
		        $output = 'Error performing update: ' . mysqli_error ($link);
		        include '../html/ConnectionFail.htm';
		        exit();
		    } 

			$sql = "DELETE FROM ForgottenPasswordRequests WHERE userID = '$userID'";
		    if(!mysqli_query($link, $sql)) {
		        $output = 'Error performing update: ' . mysqli_error ($link);
		        include '../html/ConnectionFail.htm';
		        exit();
		    }		    

		    $passwordResetSuccessful = "Password Reset";
    		include "../html/login.php";
    		exit();

		} else {
			unset($_REQUEST['email']);
			unset($_REQUEST['new_password']);
			unset($_REQUEST['token']);
			$output = 'Invalid. Please try again.';
			include '../html/login.php';
			exit();
		}
	} else {
		$output = NULL;
    	header("Location: ../html/login.php");
	}
?>