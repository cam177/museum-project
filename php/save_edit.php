<?php
	if(session_id() == '' || !isset($_SESSION)) {
        session_start();
    }
	$link = mysqli_connect('localhost', 'root', '');
	if (!$link) {
		$output = 'Unable to connect to the database server.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_set_charset($link, 'utf8')){
		$output = 'Unable to set database connection encoding.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_select_db($link, 'Museum')){
		$output = 'Unable to locate the database.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	$newRoomName;
	$newStartDate;
	$newEndDate;
	$newStartTime;
	$newEndTime;
	$newAttendees;
	$newEventType;
	$bookingID;
	$roomID;
	if(isset($_POST['submit'])){
		$newRoomName = $_POST['newRoomName'];
		$newStartDate = $_POST['newStartDate'];
		$newEndDate = $_POST['newEndDate'];
		$newStartTime = $_POST['newStartTime'];
		$newEndTime = $_POST['newEndTime'];
		$newAttendees = $_POST['newAttendees'];
		$newEventType = $_POST['newEventType'];
		$bookingID = $_POST['bookingID'];
		$roomID = $_POST['roomID'];
		
		
		$checkresult = mysqli_query($link, "SELECT ID FROM Bookings WHERE ID!='$bookingID' AND roomID = '$roomID' AND ('$newStartDate' between startDate and endDate OR '$newEndDate' between startDate and 
			endDate)");
		$rownum = mysqli_num_rows($checkresult);
		if ($rownum > 0){
			$_SESSION['dateErr'] = "yes";
			echo $bookingID;
			header('Location: ./../html/view_booking.php?bID='.$bookingID);
			//header('Location: ./../html/edit_booking.php');
		}else{
			$_SESSION['dateErr'] = "no";
			$result = mysqli_query($link, "SELECT ID FROM Rooms WHERE roomName = '$newRoomName'");
			$i = 0;
			$rownum = mysqli_num_rows($result);
			while ($i < $rownum){
		        mysqli_data_seek($result, $i);
				$row = mysqli_fetch_row($result);
				$roomID = $row[0];
				$i++;
			}

			mysqli_query($link, "UPDATE Bookings SET roomID='$roomID', startDate = '$newStartDate', endDate ='$newEndDate', startTime='$newStartTime', endTime='$newEndTime', attendees='$newAttendees', eventType='$newEventType' WHERE ID = '$bookingID'"); 

			header('Location: ./../html/user_dashboard.php');
		}

	}else if(isset($_POST['delete'])){
		$bookingID = $_POST['bookingID'];
		mysqli_query($link, "DELETE from Bookings WHERE ID = $bookingID");
		header('Location: ./../html/user_dashboard.php');
	}
?>