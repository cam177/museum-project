<?php   
if (isset($_REQUEST['name'])
	&& isset($_REQUEST['surname'])
	&& isset($_REQUEST['organization'])
	&& isset($_REQUEST['address'])
	&& isset($_REQUEST['phone_number'])
	&& isset($_REQUEST['email']) 
	&& isset($_REQUEST['password']))
{
	$name = $_REQUEST['name'];
	$surname = $_REQUEST['surname'];
	$organisation = $_REQUEST['organization'];
	$address = $_REQUEST['address'];
	$phone = $_REQUEST['phone_number'];
	$email = $_REQUEST['email'];
	$password = $_REQUEST['password'];

	$link = mysqli_connect('localhost', 'root', '');
	if (!$link) {
		$output = 'Unable to connect to the database server.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_set_charset($link, 'utf8')){
		$output = 'Unable to set database connection encoding.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_select_db($link, 'Museum')){
		$output = 'Unable to locate the database.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	$result = mysqli_query($link, "SELECT Name, Surname, organisationName, Address, phoneNumber FROM UserDetailsTable WHERE Email = '" . $email ."'");
	$i = 0;
	$rownum = mysqli_num_rows($result);
	if ($rownum == 0) {
		$errors = 0;
		$message = "";
		$pattern = "/^[a-zA-Z][a-zA-Z]*-?[a-zA-Z]*[a-zA-Z]$/";
		if (!preg_match($pattern, $name)) {
			$message = $message . "Firstname is not valid. ";
			$errors = $errors + 1;
		}
		$pattern = "/^[a-zA-Z][a-zA-Z]*-?[a-zA-Z]*[a-zA-Z]$/";
		if (!preg_match($pattern, $surname)) {
			$message = $message . "Surname is not valid. ";
			$errors = $errors + 1;
		}
		$pattern = "/^[a-zA-Z0-9\.]{1,}@{1}[a-zA-Z0-9]{1,}(\.[a-zA-Z]{2,})+/";
		if (!preg_match($pattern, $email)) {
			$message = $message . "Email is not valid. ";
			$errors = $errors + 1;
		}
		$pattern = "/^\+[0-9]{10}$/";
		if (!preg_match($pattern, $phone)) {
			$pattern = "/^[0-9]{10}$/";
			if (!preg_match($pattern, $phone)) {
				$message = $message . "Phone number is not valid. ";
				$errors = $errors + 1;
			}
		}
		$pattern = "/^[a-zA-Z0-9 ][a-zA-Z0-9\\n ]*[a-zA-Z0-9 ]$/";
		if (!preg_match($pattern, $organisation)) {
			$message = $message . "Organisation invalid. ";
			$errors = $errors + 1;
		}
		if ($errors > 0) {
			$output = $message;
			include '../html/registration.php';
			exit();	
		} else {
			$sql = "INSERT INTO UserDetailsTable SET Name = '$name', Surname = '$surname', organisationName = '$organisation', Address = '$address', phoneNumber = '$phone', 
			Email = '$email'";
			if(!mysqli_query($link, $sql)) {
				$output = 'Error performing update: ' . mysqli_error ($link);
				include '../html/ConnectionFail.htm';
				exit();
			}

			$result = mysqli_query($link, "SELECT ID FROM UserDetailsTable WHERE Email = '" . $email ."'");
			$i = 0;
			$rownum = mysqli_num_rows($result);
			while ($i < $rownum){
				mysqli_data_seek($result, $i);
				$row = mysqli_fetch_row($result);
				$uID = $row[0];
				$i++;
			}

			$role = 3;

			$hashPass = md5($password);

			$sql = "INSERT INTO Login SET userID = '$uID', Email = '$email', Password = '$hashPass', role = '$role'";
			if(!mysqli_query($link, $sql)) {
				$output = 'Error performing update: ' . mysqli_error ($link);
				include '../html/ConnectionFail.htm';
				exit();
			}
			header("Location: ../html/login.php");
		}
	} else {
		unset($_REQUEST['name']);
		unset($_REQUEST['surname']);
		unset($_REQUEST['organization']);
		unset($_REQUEST['address']);
		unset($_REQUEST['phone_number']);
		unset($_REQUEST['email']);
		unset($_REQUEST['password']);
		$output = 'User already exists.';
		include '../html/login.php';
		exit();
	}

} else {
	$output = NULL;
	header("Location: ../html/registration.php");
}
?>