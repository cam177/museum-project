<?php
require_once('./../php/config.php');

$pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);

$targetDir = "../images/";

if(isset($_POST['newRoomName']) && isset($_POST['submit'])){
        for ($i = 0; $i < count($_POST['newRoomName']); $i++) {
            $sql = "INSERT INTO Rooms (roomName, capacity, tariffPerHour, tariffPerDay, isDisabled, minimumHireTime, availableFrom, availableTo, imageName) VALUES (:roomName, :capacity, :tariffPerHour, :tariffPerDay, :isDisabled, :minHireTime, :availableFrom, :availableTo, :imageName)";
            $prep = $pdo->prepare($sql);
            $prep->bindValue(":roomName", $_POST['newRoomName'][$i]);
            $prep->bindValue(":capacity", $_POST['newCapacity'][$i]);
            $prep->bindValue(":tariffPerHour", $_POST['newPricePH'][$i]);
            $prep->bindValue(":tariffPerDay", $_POST['newPricePD'][$i]);
            $prep->bindValue(":isDisabled", $_POST['newAvailability'][$i]);
            $prep->bindValue(":minHireTime", $_POST['newMinHireTime'][$i]);
            $prep->bindValue(":availableFrom", $_POST['newAvailableFrom'][$i]);
            $prep->bindValue(":availableTo", $_POST['newAvailableTo'][$i]);
            $prep->bindValue(":imageName", basename($_FILES["newImageName"]["name"][$i]));
            $prep->execute();
        }
    }

//update
if(isset($_POST['roomName']) && isset($_POST['submit'])){
        for ($i = 0; $i < count($_POST['roomName']); $i++) {
            //update if imge is set
            if($_FILES['imageName']['name'][$i] == ""){
                $sql = "UPDATE Rooms SET roomName = :roomname, capacity = :capacity, tariffPerHour = :tariffPH, tariffPerDay = :tariffPD, isDisabled = :disabled, minimumHireTime = :minHireTime, availableFrom = :availableFrom, availableTo = :availableTo WHERE ID = :ID";

                $prep = $pdo->prepare($sql);
                $prep->bindvalue(":ID", $_POST['roomID'][$i]);
                $prep->bindvalue(":roomname", $_POST['roomName'][$i]);
                $prep->bindvalue(":capacity", $_POST['capacity'][$i]);
                $prep->bindvalue(":tariffPH", $_POST['tariffPH'][$i]);
                $prep->bindvalue(":tariffPD", $_POST['tariffPD'][$i]);
                $prep->bindvalue(":disabled", $_POST['availability'][$i]);
                $prep->bindvalue(":minHireTime", $_POST['minHireTime'][$i]);
                $prep->bindvalue(":availableFrom", $_POST['availableFrom'][$i]);
                $prep->bindvalue(":availableTo", $_POST['availableTo'][$i]);
                $prep->execute();
            } else {
                $sql = "UPDATE Rooms SET roomName = :roomname, capacity = :capacity, tariffPerHour = :tariffPH, tariffPerDay = :tariffPD, isDisabled = :disabled, minimumHireTime = :minHireTime, availableFrom = :availableFrom, availableTo = :availableTo, imageName = :imageName WHERE ID = :ID";

                $prep = $pdo->prepare($sql);
                $prep->bindvalue(":ID", $_POST['roomID'][$i]);
                $prep->bindvalue(":roomname", $_POST['roomName'][$i]);
                $prep->bindvalue(":capacity", $_POST['capacity'][$i]);
                $prep->bindvalue(":tariffPH", $_POST['tariffPH'][$i]);
                $prep->bindvalue(":tariffPD", $_POST['tariffPD'][$i]);
                $prep->bindvalue(":disabled", $_POST['availability'][$i]);
                $prep->bindvalue(":minHireTime", $_POST['minHireTime'][$i]);
                $prep->bindvalue(":availableFrom", $_POST['availableFrom'][$i]);
                $prep->bindvalue(":availableTo", $_POST['availableTo'][$i]);
                $prep->bindvalue(":imageName", basename($_FILES["imageName"]["name"][$i]));
                $prep->execute();
            }
        }
    }

foreach( $_FILES['imageName']['tmp_name'] as $index => $tmpName )
        {
            if( !empty( $tmpName ) && is_uploaded_file( $tmpName ) )
            {
                $target_file = $targetDir . basename($_FILES["imageName"]["name"][$index]);
                move_uploaded_file($tmpName, $target_file); // move to new location perhaps?
            }
        }

foreach( $_FILES['newImageName']['tmp_name'] as $index => $tmpName )
        {
            if( !empty( $tmpName ) && is_uploaded_file( $tmpName ) )
            {
                $target_file = $targetDir . basename($_FILES["newImageName"]["name"][$index]);
                move_uploaded_file($tmpName, $target_file); // move to new location perhaps?
            }
        }

header("Location: ../html/rooms.php");
?>