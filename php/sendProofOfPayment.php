<?php
	require_once ("./../swiftmailer-5.x/lib/swift_required.php");
	require_once("./../php/config.php");


    $body = "Proof of payment has been uploaded. Please find attached.";

    if ($_FILES['checklist']['size'] < 5000000) {

	    $attachment = Swift_Attachment::fromPath($_FILES['checklist']['tmp_name'])
	    		->setFilename($_FILES['checklist']['name']);

	    try {
	        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, "ssl")
	        	->setUsername('stellenboschuniversitymuseum@gmail.com')
	            ->setPassword('triggermeplenty');

	        $mailer = Swift_Mailer::newInstance($transport);

	        $message = Swift_Message::newInstance('New Booking Notification')
		        ->setFrom(array('stellenboschuniversitymuseum@gmail.com' => 'Stellenbosch University Museum'))
		        ->setTo(array(ADMIN_EMAIL))
		        ->attach($attachment)
		        ->setBody($body)
		        ->setSubject("Proof of Payment");

	        $result = $mailer->send($message);
	    } catch (Exception $e) {
	    	die("OH NO KATNISS");
	    }	
	    header('Location: ../html/user_dashboard.php?sz=yes');
	} else {

		header('Location: ../html/user_dashboard.php?sz=no');
	}
?>