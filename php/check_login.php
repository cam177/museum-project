<?php   
	if (isset($_REQUEST['email']) && isset($_REQUEST['password'])) {

		$email = $_REQUEST['email'];
		$password = $_REQUEST['password']; 

		$link = mysqli_connect('localhost', 'root', '');
		if (!$link) {
			$output = 'Unable to connect to the database server.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		if (!mysqli_set_charset($link, 'utf8')){
			$output = 'Unable to set database connection encoding.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		if (!mysqli_select_db($link, 'Museum')){
			$output = 'Unable to locate the database.';
			include '../html/ConnectionFail.htm';
			exit();
		}

		$result = mysqli_query($link, "SELECT password, role FROM Login WHERE Email = '" . $email ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		if ($rownum == 1) {
			while ($i < $rownum){
				mysqli_data_seek($result, $i);
				$row = mysqli_fetch_row($result);
				$passsql = $row[0];
				$userRole = $row[1];
				$i++;
			}
		} else {
			unset($_REQUEST['email']);
			unset($_REQUEST['password']);
			$output = 'Incorrect username/password pair. Please try again.';
			include '../html/login.php';
			exit();
		}

		$hash = md5($password);

		if ($hash != $passsql) {
			unset($_REQUEST['email']);
			unset($_REQUEST['password']);
			$output = 'Incorrect username/password pair. Please try again.';
			include '../html/login.php';
			exit();
		}

		$result = mysqli_query($link, "SELECT ID, Name, Surname, organisationName, Address, phoneNumber FROM UserDetailsTable WHERE Email = '" . $email ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		while ($i < $rownum){
			mysqli_data_seek($result, $i);
			$row = mysqli_fetch_row($result);
			$userid = $row[0];
			$userName = $row[1];
			$userSurname = $row[2];
			$userOrgName = $row[3];
			$userAddress = $row[4];
			$userPhoneNum = $row[5];
			$i++;
		}

		session_start();
		$_SESSION['userid'] = $userid;

	    $result = mysqli_query($link, "SELECT NameOfRole FROM Roles WHERE Role = '" . $userRole ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		while ($i < $rownum){
			mysqli_data_seek($result, $i);
			$row = mysqli_fetch_row($result);
			$roleName = $row[0];
			$i++;
		}

	    if ($roleName == "Director") {
	        $_SESSION['userRole'] = "Director";
	        header("Location: ../html/Admin.php");
	    } else if ($roleName == "Admin") {
	        $_SESSION['userRole'] = "Admin";
	        header("Location: ../html/Admin.php");
	    } else if ($roleName == "User") {
	    	$_SESSION['userRole'] = "User";
	    	header("Location: ../html/user_dashboard.php");
	    } else {
	    	header("Location: ../html/ConnectionFail.htm");
	    }
    } else {
    	$output = NULL;
    	header("Location: ../html/login.php");
    }
?>