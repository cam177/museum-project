<?php
require_once("./../php/config.php");
$pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
$sql = "SELECT Count(UserDetailsTable.Name) AS Count FROM UserDetailsTable,Login WHERE UserDetailsTable.ID = Login.userID AND Login.role = 3";
$prep = $pdo->prepare($sql);
$prep->execute();
$rows1 = array();
$rows1['Name'] = 'Num of Users';
while($rr = $prep->fetch(PDO::FETCH_ASSOC)) {
    $rows1['data'][] = $rr['Count'];
}

$result = array();
array_push($result,$rows1);

print json_encode($result, JSON_NUMERIC_CHECK);
?>