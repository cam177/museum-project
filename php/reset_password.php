<?php   

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return md5($randomString);
	}

	$email = $_REQUEST['email'];
	
	$link = mysqli_connect('localhost', 'root', '');
	if (!$link) {
		$output = 'Unable to connect to the database server.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_set_charset($link, 'utf8')){
		$output = 'Unable to set database connection encoding.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	if (!mysqli_select_db($link, 'Museum')){
		$output = 'Unable to locate the database.';
		include '../html/ConnectionFail.htm';
		exit();
	}

	$result = mysqli_query($link, "SELECT * FROM Login WHERE Email = '" . $email ."'");
	$i = 0;
	$rownum = mysqli_num_rows($result);
	if ($rownum != 1) {
		$outputReset = 'Email not found. Please Register';
		include '../html/registration.php';
		exit();	
	} else {
		$emails_file = fopen("../emailsLog/emailsFile.mail", "a");
		$randomString = generateRandomString();
		while ($i < $rownum){
			mysqli_data_seek($result, $i);
			$row = mysqli_fetch_row($result);
			$uID = $row[0];
			$i++;
		}
		$txt = $uID . "/" . $randomString . "\n";
		fwrite($emailsFile, $txt);
		fclose($emailsFile);

		$result = mysqli_query($link, "SELECT * FROM ForgottenPasswordRequests WHERE userID = '" . $uID ."'");
		$i = 0;
		$rownum = mysqli_num_rows($result);
		if ($rownum == 1) {
			header("Location:  ../html/login.php?a=0");
		} else {
			$sql = "INSERT INTO ForgottenPasswordRequests SET userID = '$uID', resetToken = '$randomString'";
		    if(!mysqli_query($link, $sql)) {
		        $output = 'Error performing update: ' . mysqli_error ($link);
		        include '../html/ConnectionFail.htm';
		        exit();
		    }
		    header("Location:  ../html/login.php?a=1");							
		}
	}
?>