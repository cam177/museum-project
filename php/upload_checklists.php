<?php
require_once("./../php/config.php");
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}

$target_dir = "./../checklists_uploads/";
$target_file = $target_dir . basename($_FILES["checklist"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["checklist"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["checklist"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "pdf") {
    echo "Sorry, only JPG and PDF files are allowed.<br>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["checklist"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["checklist"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
if($uploadOk == 1) {
try {
    $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
    $sql = "UPDATE Bookings SET uploadedChecklist = '$target_file' WHERE ID = :ID";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(":ID",$_SESSION['BookingID']);            
    $prep->execute();

    $_SESSION['msg'] = "Your image upload was succeesful";
    /*include "../html/manage_bookings.php";
    exit();*/
    header("Location: ../html/manage_bookings.php");


} catch (PDOException $e) {
    die($e->getMessage());
}
} else {

}
?>