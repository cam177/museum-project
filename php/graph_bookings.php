<?php
require_once("./../php/config.php");
$pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
$sql = "SELECT UserDetailsTable.Name, Count(*) AS Count FROM Bookings,UserDetailsTable WHERE Bookings.userID = UserDetailsTable.ID GROUP BY Bookings.userID";
$prep = $pdo->prepare($sql);
$prep->execute();
$rows1 = array();
while($rr = $prep->fetch(PDO::FETCH_ASSOC)) {
	$rows1['Name'][] = $rr['Name'];
    $rows1['data'][] = $rr['Count'];
}

$result = array();
array_push($result,$rows1);

print json_encode($result, JSON_NUMERIC_CHECK);
?>