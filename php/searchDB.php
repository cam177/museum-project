<?php
$output = $_POST['dataVal'];

$users = array();
require_once('./../php/config.php');
try{
    $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM UserDetailsTable,Login WHERE UserDetailsTable.ID = Login.userID AND Login.role = 3 AND UserDetailsTable.Name LIKE CONCAT(:name, '%')";
    $prep = $pdo->prepare($sql);
    $prep->bindValue(":name", $output);
    $prep->execute();
    while($row = $prep->fetch()) {
        array_push($users, $row['Name']);
    }
    $pdo = NULL;
}
catch(PDOException $e) {            
}
$i=0;
foreach($users as $user) {
    
    echo "<li><a id=$i onclick=run($i)>$user</a></li>";
    $i++;                                                        
}  

?>
