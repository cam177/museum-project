<?php

    require_once("./../php/config.php");

    function findRooms() {
        try {
            $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
            $sql = "SELECT `ID`, `roomName` FROM `Rooms`";
            $prep = $pdo->prepare($sql);
            $prep->execute();

            while ($row = $prep->fetch(PDO::FETCH_ASSOC)) {
                echo '<li><a href="#" class="room" id="'.$row['ID'].'">'.$row['roomName'].'</a></li>';
            }
        } catch (PDOException $e) {
            die($e->getMessage());
        }  
    }

?>