<?php
    require_once('./../php/config.php');
    session_start();
    if (!isset($_SESSION['userid']) ) {
        header("Location: ../html/login.php");
    }
    if (!isset($_SESSION['userRole']) ) {
        header("Location: ../html/index.php");
    } else {
        if (!($_SESSION['userRole'] == "Admin" || $_SESSION['userRole'] == "Director")) {
            header("Location: ../html/index.php");
        }
    }
    $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
    $sql = "Select * FROM Rooms";
    $rooms = $pdo->query($sql);
    
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<title>Museum Website</title>

	<link rel="stylesheet" href="./../css/bootstrap.min.css"/>
<link href='../fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='../css/bookings.css' rel='stylesheet' />
<link href='../css/general.css' rel='stylesheet' />
<script src='../lib/moment.min.js'></script>
<script src='../lib/jquery.min.js'></script>
<script src='../js/rooms.js'></script>
</head>
    
<body>
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li><a id="login-btn" href="../php/logout.php">Log Out</a></li>
                </ul>
                <span class=pull-left>
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="Admin.php"><i class="glyphicon glyphicon-home"></i> Admin Portal</a>
                        </li>
                        <?php
                            if($_SESSION['userRole'] == "Director") {
                            echo '<li >';
                                echo '<a href="Create_new_admin.php"><i class="fa fa-fw fa-dashboard"></i> Create new admin</a>';
                            echo '</li>';
                            }
                        ?>
                        <li>
                            <a href="manage_bookings.php"><i class="fa fa-fw fa-dashboard"></i> Manage Bookings</a>
                        </li>
                        <li class="active">
                            <a href="rooms.php"><i class="fa fa-fw fa-dashboard"></i> Manage Rooms</a>
                        </li>
                        <li>
                            <a href="reports.php"><i class="fa fa-fw fa-dashboard"></i> View Reports</a>
                        </li>
                    </ul>
                </span>
			</div>
        </nav>
        </div>
    </div>

     <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Rooms</h3>
                </div>
                <div class="panel-body">
                    <form action="./../php/update_rooms.php" method="POST" id='form-rooms' enctype="multipart/form-data">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Capacity</th>
                                    <th>Price per hour</th>
                                    <th>Price per day</th>
                                    <th>Availability</th>
                                    <th>Min hire time</th>
                                    <th>Available from</th>
                                    <th>Available to</th>
                                    <th>Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 0;
                                    while($room = $rooms->fetch()){
                                        $availability;
                                        $disabled;
                                        if ($room['isDisabled'] == '0'){
                                            $availability = 'available';
                                            $disabled = 0;
                                        } else {
                                            $availability = 'unavailable';
                                            $disabled = 1;
                                        }

                                        echo
                                        "<tr id='".$room['ID']."' class='editable updated' >".
                                            "<input type='hidden' name='roomID[]' value='".$room['ID']."'></input>".
                                            "<input type='hidden' name='availability[]' class='hiddenF' value='".$disabled."'></input>".
                                            "<td><input type='text' name='roomName[]' class='table-input name' value='".$room['roomName']."' ></input></td>".
                                            "<td><input type='number' name='capacity[]' min='0' class='table-num capacity' value='".$room['capacity']."' ></input></td>".
                                            "<td>R <input type='number' name='tariffPH[]' min='0' class='table-num pricePH' value='".$room['tariffPerHour']."' ></input></td>".
                                            "<td>R <input type='number' name='tariffPD[]' min='0' class='table-num pricePD' value='".$room['tariffPerDay']."' ></input></td>".
                                            "<td class='availabilty ".$availability." clickable-lbl'>".$availability."</td>".
                                            "<td><input type='number' name='minHireTime[]' min='0' class='table-num minHireTime' value='".$room['minimumHireTime']."' ></input></td>".
                                            "<td><input type='time' step='1' name='availableFrom[]' class='table-input availableFrom' value='".$room['availableFrom']."' ></input></td>".
                                            "<td><input type='time' step='1' name='availableTo[]' class='table-input availableTo' value='".$room['availableTo']."' ></td>".
                                            "<td><input type='file' name='imageName[]' class='table-input image' value='".$room['imageName']."'></input></td></tr>";
                                    }
                                ?>
                                <tr id="new-room"><td colspan="9"><span class="glyphicon glyphicon-plus"></span>Add room</td></tr>
                            </tbody>
                        </table>
                        <input type="submit" class="btn btn-success" id="submit" name="submit">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</body>
</html>