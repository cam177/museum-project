<!DOCTYPE html>
<?php
    if (isset($_GET['token'])) {
        $link = mysqli_connect('localhost', 'root', '');
        if (!$link) {
            $output = 'Unable to connect to the database server.';
            include 'ConnectionFail.htm';
            exit();
        }

        if (!mysqli_set_charset($link, 'utf8')){
            $output = 'Unable to set database connection encoding.';
            include 'ConnectionFail.htm';
            exit();
        }

        if (!mysqli_select_db($link, 'Museum')){
            $output = 'Unable to locate the database.';
            include 'ConnectionFail.htm';
            exit();
        }

        $token = $_GET['token'];

        $result = mysqli_query($link, "SELECT userID FROM ForgottenPasswordRequests WHERE resetToken = '" . $token ."'");
        $i = 0;
        $rownum = mysqli_num_rows($result);
        if ($rownum == 1) {
            while ($i < $rownum){
                mysqli_data_seek($result, $i);
                $row = mysqli_fetch_row($result);
                $userID = $row[0];
                $i++;
            }
        } else {
            $output = 'Invalid Password Reset Token Provided';
            include 'ConnectionFail.htm';
            exit();
        }
    } else {
        $output = 'No password reset token provided.';
        include 'ConnectionFail.htm';
        exit();
    }
?>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>Stellenbsoch University Museum</title>
    	<link rel="stylesheet" href="./../css/bootstrap.min.css">
        <link rel="stylesheet" href="./../css/general.css">
        <link rel="stylesheet" href="./../css/reset_password.css">
    </head>
    
    <body>
        <header></header>
        <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li class="middle-class"><a id="register-btn" href="../html/registration.php">Register</a></li>
                    <li class="middle-class"><a id="login-btn" href="../html/login.php">Log In</a></li>
                </ul>
            </div>
        </nav>
        <div class="page-header">
            <h1>Reset Password</h1>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <div class="panel panel-default half-side-panel">
                    <h3 class="panel-header">Password Reset Details</h3>
                    <div class="panel-body">
                        <form id="password-reset-form" method="POST" action="../php/rewritePasswordToDb.php" class="form-horizontal" role="form" accept-charset="UTF-8">
                            <div class="form-group">
                                <label for="email" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="email" name="email" type="email" required>
                                </div>
                            </div>
                            <?php
                                if (isset($_GET['pw'])) {
                                    if ($_GET['pw'] == 'no') {
                                        echo '<div class="form-group"><div class="col-xs-5"></div><div class="col-xs-7"><h4 class="error-message">Password do not match</h4></div></div>';
                                        unset($_GET['pw']);
                                    }
                                }
                            ?>
                            <div class="form-group">
                                <label for="new_password" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">New Password:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="new_password" name="new_password" type="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="new_password_confirm" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">New Password Confirm:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="new_password_confirm" name="new_password_confirm" type="password" required>
                                </div>
                            </div>
                            <?php
                                if (isset($token)) {
                                    echo '<input id="token" name="token" type="hidden" value="' . $token .'" required>';
                                }
                            ?>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                            <button class="btn btn-success submit-btn" type="submit">Reset Password</button>
                        </form>
                    </div>
                </div>                
            </div>
        </div>
    </body>
</html>