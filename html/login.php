<!DOCTYPE html>
<?php
    if (isset($_GET['a'])) {
        if ($_GET['a'] == 1) {
            $string = "Password Reset Email Sent";
        } else if ($_GET['a'] == 0) {
            $string = "Email Not Sent. Token already exists";
        }
        unset($_GET['a']);
    }
?>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>Stellenbsoch University Museum</title>
    	<link rel="stylesheet" href="./../css/bootstrap.min.css">
        <link rel="stylesheet" href="./../css/general.css">
        <link rel="stylesheet" href="./../css/login.css">
        <script src="./../js/jquery-1.12.2.min.js"></script>
        <script src="./../js/bootstrap.min.js"></script>
    </head>
    
    <body>
        <header></header>
        <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li class="middle-class"><a id="register-btn" href="../html/registration.php">Register</a></li>
                    <li class="middle-class"><a id="login-btn" href="../html/login.php">Log In</a></li>
                </ul>
            </div>
        </nav>
        <div class="page-header">
            <h1>Login Page</h1>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <div class="panel panel-default half-side-panel">
                    <h3 class="panel-header">Login Details</h3>
                    <div class="panel-body">
                        <form id="login-form" method="POST" action="../php/check_login.php" class="form-horizontal" role="form" accept-charset="UTF-8">
                            <?php
                                if (isset($output)) {
                                    echo '<div class="form-group"><div class="col-xs-3"></div><div class="col-xs-7"><h4 class="error-message">' . $output . '</h4></div></div>';
                                } else if (isset($string)) {
                                    echo '<div class="form-group"><div class="col-xs-3"></div><div class="col-xs-7"><h4 class="success-message">' . $string . '</h4></div></div>';
                                } else if (isset($passwordResetSuccessful)) {
                                    echo '<div class="form-group"><div class="col-xs-3"></div><div class="col-xs-7"><h4 class="success-message">' . $passwordResetSuccessful . '</h4></div></div>';
                                }
                            ?>
                            <div class="form-group">
                                <label for="email" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="email" name="email" type="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Password:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="password" name="password" type="password" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                            <button class="btn btn-success submit-btn" type="submit">Login</button>
                        </form>
                        <br>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                        <button type="button" class="btn btn-warning btn-md" data-toggle="modal" data-target="#resetPasswordModal">Forgot Your Password?</button>
                        <div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Reset Password?</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="reset-password-form" method="POST" action="../php/passwordReset.php" class="form-horizontal" role="form" accept-charset="UTF-8">
                                            <div class="form-group">
                                                <label for="email" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email:</label>
                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                    <input class="form-control" id="email" name="email" type="email" required>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                                            <button class="btn btn-success submit-btn" type="submit">Send Reset Email</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </body>
</html>