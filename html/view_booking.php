 <?php
 if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['userid']) ) {
 header("Location: ../html/login.php");
}
if (!isset($_SESSION['userRole']) ) {
 header("Location: ../html/index.php");
} else {
 if (!($_SESSION['userRole'] == "User")) {
     header("Location: ../html/index.php");
 }
}
if(!isset($_GET['bID'])) { 
    header("Location: ../html/user_dashboard.php");
}


$link = mysqli_connect('localhost', 'root', '');
if (!$link) {
    $output = 'Unable to connect to the database server.';
    include 'ConnectionFail.htm';
    exit();
}

if (!mysqli_set_charset($link, 'utf8')){
    $output = 'Unable to set database connection encoding.';
    include 'ConnectionFail.htm';
    exit();
}

if (!mysqli_select_db($link, 'Museum')){
    $output = 'Unable to locate the database.';
    include 'ConnectionFail.htm';
    exit();
}
$bookingID = $_GET['bID'];

$result = mysqli_query($link, "SELECT * FROM Bookings WHERE ID = '$bookingID'");
$i = 0;
$roomID;
$startDate;
$endDate;
$startTime;
$endTime;
$attendees;
$eventType;
$cost;
$totalHours;
$approved;
$rownum = mysqli_num_rows($result);
while ($i < $rownum){
 mysqli_data_seek($result, $i);
 $row = mysqli_fetch_row($result);
 $roomID = $row[2];
 $startDate = $row[3];
 $endDate = $row[4];
 $startTime = $row[5];
 $endTime = $row[6];
 $attendees = $row[7];
 $eventType = $row[8];
 $cost = $row[9];
 $totalHours = $row[10];
 $approved = $row[11];
 $i++;
}


    //SELECT roomName, imageName FROM Rooms WHERE ID = '$roomID'

$roomName;
$image;
$nameResult = mysqli_query($link, "SELECT roomName, imageName FROM Rooms WHERE ID = '$roomID'");
$j = 0;
$rownumj = mysqli_num_rows($nameResult);
while ($j < $rownumj){
 mysqli_data_seek($nameResult, $j);
 $row = mysqli_fetch_row($nameResult);
 $roomName = $row[0];
 $image = "./../images/$row[1]";
 $j++;
}

list($sy, $sm, $sd) = explode('-', $endDate);

$endY = $sy;
$endM = $sm;
$endD = $sd;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Museum Website</title>
    <link rel="stylesheet" href="./../css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link rel="stylesheet" href="./../css/view_edit.css">
    <script src='./../lib/moment.min.js'></script>
    <script src='./../lib/jquery.min.js'></script>
    <script src="./../js/bootstrap.min.js"></script>
    <script src="./../js/view_booking.js"></script>
    <script type="text/javascript"><?php echo("var startDate = '$startDate'; var endY = '$endY'; var endM = '$endM'; var endD = '$endD'") ?></script>

    <script type="text/javascript" src="./../js/bootstrap-datepicker.min.js"></script>
</head>
<body>
 <header></header>
 <nav class="navbar navbar-default navbar-custom" role="navigation">
     <div class="navbar-header" id="nav-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
             <span class="sr-only">Navigation Toggle</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
         </button>
         <img class="logo-img" src="./../images/logo.jpg"/>
         <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
     </div>
     <div class="collapse navbar-collapse" id="navbar-collapse-button">  
         <ul class="nav navbar-nav">
             <li><a id="login-btn" href="../php/logout.php">Log Out</a></li>
         </ul>
         <span class=pull-left>
             <ul class="nav navbar-nav">
                 <li>
                     <a href="user_dashboard.php"><i class="glyphicon glyphicon-home"></i> User Portal</a>
                 </li>
                 <li>
                     <a href="new_booking.php"><i class="fa fa-fw fa-dashboard"></i>New Booking</a>
                 </li>
             </ul>
         </span>
     </div>
 </nav>
 <?php

       if (isset($_SESSION['dateErr'])) {
            echo '<div class="row">';
            echo '<div class="col-lg-1"></div>';
            echo '<div class="col-lg-10 col-md-12">';
            echo '<div class="alert alert-warning text-center">';
            echo '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>';
            if($_SESSION['dateErr'] == "yes") {
                echo 'There was a date overlap';
            }
            echo '</div>';
            echo '</div>';
            echo '</div>';
            unset($_SESSION['dateErr']);
        }

        ?>
 <div class="page-header">
     <h1>View Booking</h1>
 </div>
 <div class="row" id="Info-Row">
     <div class="col-lg-6">
         <div class="panel panel-default stats-pnl">                    
             <div class="panel-heading">
                 <h3 class="panel-title">
                     Booking Information
                 </h3>
             </div>
             <div class="panel-body">
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Room:</h4> 
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="roomName"><?php echo("$roomName")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Start Date:</h4> 
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="Sdate"><?php echo("$startDate")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">End Date:</h4> 
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="Edate"><?php echo("$endDate")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Start Time:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="Stime"><?php echo("$startTime")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">End Time:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="Etime"><?php echo("$endTime")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Attendees:</h4>                                            
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="attendees"><?php echo("$attendees")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Event Type:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="type"><?php echo("$eventType")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Cost:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="cost"><?php echo("R $cost,00")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Total Hours:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="hours"><?php echo("$totalHours")?></h4>
                     </div>
                 </div>
                 <div class="row">
                     <div class='col-sm-4 contact'>
                         <h4 class="text-right">Approved:</h4>
                     </div>
                     <div class='col-sm-6 contact'>
                         <h4 id="approved">
                         <?
                         if ($approved == 0){
                            echo("Yes");
                        } else if($approved == 1) {
                            echo("No");
                        } else if($approved == 2){
                            echo("Pending");
                        }
                        ?>
                        </h4>
                     </div>
                 </div>
             </div>
             <div class="panel-footer clearfix">
                 <span class=pull-right>
                     <form action="edit_booking.php" method="post">
                         <input type="hidden" name="roomName" value=<?php echo("'$roomName'")?>>
                         <input type="hidden" name="startDate" value=<?php echo("'$startDate'")?>>
                         <input type="hidden" name="endDate" value=<?php echo("'$endDate'")?>>
                         <input type="hidden" name="startTime" value=<?php echo("'$startTime'")?>>
                         <input type="hidden" name="endTime" value=<?php echo("'$endTime'")?>>
                         <input type="hidden" name="attendees" value=<?php echo("'$attendees'")?>>
                         <input type="hidden" name="eventType" value=<?php echo("'$eventType'")?>>
                         <input type="hidden" name="roomID" value=<?php echo("$roomID")?>>
                         <input type="hidden" name="bookingID" value=<?php echo("$bookingID")?>>
                         <input type="hidden" name="approved" value=<?php echo("$approved")?>>
                         <input type="hidden" name="image" value=<?php echo("$image")?>>
                         <input type="submit" class="btn btn-default" value="Edit or Cancel Booking" id="edit">
                     </form>
                 </span>
                 <span class="pull-left">
                    <button type ="button" id="upload-but" name="upload" data-toggle="modal" data-target="#uploadPaymentModal" class="btn btn-primary btn-information">Upload Proof of Payment</button>
                 </span>
             </div>
         </div>
     </div>
     <div class="col-lg-6">
         <div class="panel panel-default stats-pnl">
             <div class="panel-heading">
                 <h3 class="panel-title">
                     Room
                 </h3>
             </div>
             <div class="image-panel-body">
                 <img class="img-responsive" src=<?php echo("$image") ?> alt="Chania">
             </div>
         </div>
     </div>

     <div class="col-lg-1"></div>
     <div class="modal fade" id="uploadPaymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Proof of Payment</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadChecklistForm" method="POST" action="../php/sendProofOfPayment.php" class="form-horizontal" role="form" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="checklist" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Proof of Payment:</label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input class="form-control" id="payment" name="checklist" type="file" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                        <button class="btn btn-success submit-btn" type="submit">Upload</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
</div>
</body>
</html>