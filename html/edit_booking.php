 <!DOCTYPE html>
 <?php
     if(session_id() == '' || !isset($_SESSION)) {
        session_start();
    }
     if (!isset($_SESSION['userid']) ) {
         header("Location: ../html/login.php");
     }
     if (!isset($_SESSION['userRole']) ) {
         header("Location: ../html/index.php");
     } else {
         if (!($_SESSION['userRole'] == "User")) {
             header("Location: ../html/index.php");
         }
     }

    $link = mysqli_connect('localhost', 'root', '');
    if (!$link) {
        $output = 'Unable to connect to the database server.';
        include 'ConnectionFail.htm';
        exit();
    }

    if (!mysqli_set_charset($link, 'utf8')){
        $output = 'Unable to set database connection encoding.';
        include 'ConnectionFail.htm';
        exit();
    }

    if (!mysqli_select_db($link, 'Museum')){
        $output = 'Unable to locate the database.';
        include 'ConnectionFail.htm';
        exit();
    }

     $roomName;
     $startDate;
     $endDate;
     $startTime;
     $endTime;
     $attendees;
     $eventType;
     $image;
     $roomID;
     $bookingID;
     $approved;
     $image;
 
     if (isset($_POST['roomName'])){
         $roomName = $_POST['roomName'];
         $startDate = $_POST['startDate'];
         $endDate = $_POST['endDate'];
         $startTime = $_POST['startTime'];
         $endTime = $_POST['endTime'];
         $attendees = $_POST['attendees'];
         $eventType = $_POST['eventType'];
         $roomID = $_POST['roomID'];
         $bookingID = $_POST['bookingID'];
         $approved = $_POST['approved'];
         $image = $_POST['image'];
     }else{
        header("Location: ../html/user_dashboard.php");
     }
 
     list($sy, $sm, $sd) = explode('-', $startDate);
 
     $startY = $sy;
     $startM = $sm;
     $startD = $sd;
 ?>
 
 <html>
     <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Museum Website</title>
        <link rel="stylesheet" href="./../css/bootstrap.min.css">
         <link rel="stylesheet" href="./../css/general.css">
         <link rel="stylesheet" href="./../css/view_edit.css">
         <script type="text/javascript" src="./../js/jquery-1.12.2.js"></script>
         <script type="text/javascript"><?php echo("var startDate = '$startDate'; var startY = '$startY'; var startM = '$startM'; var startD = '$startD'") ?></script>
         <script type="text/javascript" src="./../js/view_edit.js"></script>
         <script type="text/javascript" src="./../js/bootstrap.min.js"></script>
         <script type="text/javascript" src="./../js/bootstrap-datepicker.min.js"></script>
     </head>
     
     <body>
         <header></header>
         <nav class="navbar navbar-default navbar-custom" role="navigation">
             <div class="navbar-header" id="nav-header">
                 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                     <span class="sr-only">Navigation Toggle</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                 </button>
                 <img class="logo-img" src="./../images/logo.jpg"/>
                 <a class="navbar-brand" id="nav-title" href="../php/index.php">Stellenbsoch University Museum</a>
             </div>
             <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                 <ul class="nav navbar-nav">
                     <li><a id="login-btn" href="../php/logout.php">Log Out</a></li>
                 </ul>
                 <span class=pull-left>
                     <ul class="nav navbar-nav">
                         <li>
                             <a href="user_dashboard.php"><i class="glyphicon glyphicon-home"></i> User Portal</a>
                         </li>
                         <li>
                             <a href="new_booking.php"><i class="fa fa-fw fa-dashboard"></i>New Booking</a>
                         </li>
                     </ul>
                 </span>
            </div>
         </nav>

         <div class="page-header">
             <h1>Edit or Cancel Booking</h1>
         </div>
         <div class="row" id="Info-Row">
             <div class="col-lg-6">
                 <div class="panel panel-default stats-pnl">                    
                     <div class="panel-heading">
                         <h3 class="panel-title">
                             Room Information
                         </h3>
                     </div>
                  <form class="form-horizontal" id="form-edit" action="../php/save_edit.php" method="post">
                     <div class="panel-body">
                             <div class="form-group">
                               <label class="col-sm-3 control-label" id="room">Room:</label>
                               <div class="col-sm-3" id="room-group">
                                   <select class="form-control" name="newRoomName">
                                    <?php
                                       $result = mysqli_query($link, "SELECT roomName FROM Rooms");
                                        $i = 0;
                                        $rownum = mysqli_num_rows($result);
                                        while ($i < $rownum){
                                         mysqli_data_seek($result, $i);
                                         $row = mysqli_fetch_row($result);
                                         echo("<option id=$row[0]>$row[0]</option>");
                                         $i++;
                                    }
                                    ?>                          
                                   </select>
                               </div>
                             </div>
                             <div class="form-group">
                               <label class="col-sm-3 control-label">Start Date:</label>
                                 <div class="col-sm-3">
                                     <input class="form-control date" name="newStartDate" value=<?php echo("'$startDate'")?> type="text"/>
                                 </div>
                             </div>
                             <div class="form-group">
                               <label class="col-sm-3 control-label">End Date:</label>
                                 <div class="col-sm-3">
                                     <input class="form-control date" name="newEndDate" value=<?php echo("'$endDate'")?> type="text"/>
                                      
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label col-sm-3">Start Time:</label>
                                 <div class="col-sm-3">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                         <input id="timepicker1" type="text" class="form-control input-small start_time" name="newStartTime" value=<?php echo("'$startTime'")?>>
                                         <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                     </div>
                                     <div class="alert alert-danger alert_start_time collapse">Format: <strong>HH:MM</strong></div>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label col-sm-3">End Time:</label>
                                 <div class="col-sm-3">
                                    <div class="input-group bootstrap-timepicker timepicker">                                        
                                         <input id="timepicker1" type="text" class="form-control input-small end_time" name="newEndTime" value=<?php echo("'$endTime'")?>>
                                         <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                     </div>
                                     <div class="alert alert-danger alert_end_time collapse">Format: <strong>HH:MM</strong></div>
                                 </div>
                             </div>
                            <div class="form-group">
                               <label class="col-sm-3 control-label">Number of Attendees:</label>
                                 <div class="col-sm-3">
                                     <input class="form-control" name="newAttendees" value=<?php echo("'$attendees'")?> type="text"/>
                                 </div>
                             </div>
                             <div class="form-group">
                               <label class="col-sm-3 control-label" id="newEventType">Event Type:</label>
                               <div class="col-sm-3" id="room-group">
                                   <select class="form-control" name="newEventType">
                                     <option id="Conference">Conference</option>
                                     <option id="Lecture">Lecture</option>
                                     <option id="Party">Party</option>
                                     <option id="Other">Other</option>
                                   </select>
                               </div>
                             </div>                            
                      </div>
                     <div class="panel-footer clearfix">
                         <span class=pull-right>
                             <input type="hidden" name="bookingID" value=<?php echo("'$bookingID'")?>>
                             <input type="hidden" name="startDate" value=<?php echo("'$startDate'")?>>
                             <input type="hidden" name="endDate" value=<?php echo("'$endDate'")?>>
                             <input type="hidden" name="roomID" value=<?php echo("'$roomID'")?>>
                             <input type="submit" class="btn btn-default" id="save_btn" name="submit" value="Save Changes">
                         </span>
                  </form>
                      <span class=pull-left>
                         <form action="../php/save_edit.php" method="post">
                                 <!-- Trigger the modal with a button -->
                                   <button type="button" class="btn btn-default" id="cancel_btn" data-toggle="modal" data-target="#myModal">Cancel Booking</button>
                                   <!-- Modal -->
                                   <div class="modal fade" id="myModal" role="dialog">
                                     <div class="modal-dialog modal-sm">
                                       <div class="modal-content">
                                         <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                           <h4 class="modal-title">Are you sure you want to cancel?</h4>
                                         </div>
                                         <div class="modal-body">
                                             <span>
                                                 <input type="hidden" name="bookingID" value=<?php echo("'$bookingID'")?>>
                                                 <input type="submit" class="btn btn-danger" style="width: 100px;" name="delete" value="Yes">
                                             </span>
                                              <span class="pull-right">
                                                 <button type="button" class="btn btn-success" data-dismiss="modal" style="width: 100px;">No</button>
                                             </span>
                                         </div>
                                       </div>
                                     </div>
                                 </div>
                             </form>
                         </span>
                     </div>
                 </div>
             </div>
             <div class="col-lg-6">
                 <div class="panel panel-default stats-pnl">
                     <div class="panel-heading">
                         <h3 class="panel-title">
                             Room
                         </h3>
                     </div>
                     <div class="image-panel-body">
                         <img class="img-responsive" src=<?php echo("$image") ?> alt="Chania">
                     </div>
                 </div>
             </div>
         
     <script>
         $(document).ready(function(){
             document.getElementById(<?php echo("'$roomName'"); ?>).selected = "true";
             document.getElementById(<?php echo("'$eventType'"); ?>).selected = "true";
 
             var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
             $('.date').datepicker({
                 format: 'yyyy-mm-dd',
                 container: container,
                 todayHighlight: true,
                 autoclose: true,
             })
         })
     </script>        
 </body>
 </html>
