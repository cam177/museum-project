<?php
require_once('./../php/config.php');
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['userid']) ) {
    header("Location: ../html/login.php");
}
if (!isset($_SESSION['userRole']) ) {
    header("Location: ../html/index.php");
} else {
    if (!($_SESSION['userRole'] == "Admin" || $_SESSION['userRole'] == "Director")) {
        header("Location: ../html/index.php");
    }
}

$pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);

if(isset($_POST['action'])){
    if($_POST['action'] == 'approved'){
        $sql = "UPDATE Bookings SET approved=0 WHERE ID = ".$_POST['event'];
        $pdo->query($sql);
    } else if($_POST['action'] == 'rejected'){
        $sql = "UPDATE Bookings SET approved=1 WHERE ID = ".$_POST['event'];
        $pdo->query($sql);
    }
}


if(isset($_GET["bID"])) {
    $_SESSION['BookingID'] = $_GET["bID"];
}

$events = [];
$sql = "SELECT * FROM Bookings";
$result = $pdo->query($sql);
while($eventInfo = $result->fetch()){
    $sql = "SELECT Email, Name, Surname FROM UserDetailsTable WHERE ID = ". $eventInfo["userID"];
    $emails = $pdo->query($sql);
    $email = $emails->fetch();
    $eventInfo["email"] = $email["Email"];
    $eventInfo["name"] = $email["Name"];
    $eventInfo["surname"] = $email["Surname"];
    $sql = "SELECT roomName FROM Rooms WHERE ID = ". $eventInfo["roomID"];
    $rooms = $pdo->query($sql);
    $room = $rooms->fetch();
    $eventInfo["room"] = $room["roomName"];
    $events[] = $eventInfo;
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<title>Museum Website</title>

	<link rel="stylesheet" href="./../css/bootstrap.min.css"/>

    <link href='../css/fullcalendar.css' rel='stylesheet' />
    <link href='../css/bookings.css' rel='stylesheet' />
    <link href='../css/general.css' rel='stylesheet' />
    <link href='../css/manage_bookings.css' rel='stylesheet' />

    <script src='../lib/moment.min.js'></script>
    <script src='../lib/jquery.min.js'></script>
    <script src='../js/fullcalendar.js'></script>
    <script src='../js/manage_bookings.js'></script>
    <script src="../js/bootstrap.min.js"></script>
    <script type="text/javascript"><?php echo "var events = ". json_encode($events). ";";?> </script>

</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-default navbar-custom" role="navigation">
                <div class="navbar-header" id="nav-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                        <span class="sr-only">Navigation Toggle</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img class="logo-img" src="./../images/logo.jpg"/>
                    <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                    <ul class="nav navbar-nav">
                        <li><a id="register-btn" href="./../php/logout.php">Log Out</a></li>
                    </ul>
                    <span class=pull-left>
                       <ul class="nav navbar-nav">
                        <li>
                            <a href="Admin.php"><i class="glyphicon glyphicon-home"></i> Admin Portal</a>
                        </li>
                        <?php
                        if($_SESSION['userRole'] == "Director") {
                            echo '<li >';
                            echo '<a href="Create_new_admin.php"><i class="fa fa-fw fa-dashboard"></i> Create new admin</a>';
                            echo '</li>';
                        }
                        ?>
                        <li class="active">
                            <a href="manage_bookings.php"><i class="fa fa-fw fa-dashboard"></i> Manage Bookings</a>
                        </li>
                        <li>
                            <a href="rooms.php"><i class="fa fa-fw fa-dashboard"></i> Manage Rooms</a>
                        </li>
                        <li>
                            <a href="reports.php"><i class="fa fa-fw fa-dashboard"></i> View Reports</a>
                        </li>
                    </ul>
                </span>
            </div>
        </nav>
    </div>
</div>
<?php


if (isset($_SESSION['msg'])) {
    echo '<div class="row">';
    echo '<div class="col-lg-1"></div>';
    echo '<div class="col-lg-10 col-md-12">';
    echo '<div class="alert alert-success text-center">';
    echo '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>';
    echo $_SESSION['msg'];
    echo '</div>';
    echo '</div>';
    echo '</div>';
    unset($_SESSION['msg']);
}


?>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-5 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Calendar</h3>
            </div>
            <div class="panel-body">
                <div id="calendar"></div>
            </div>
        </div>

    </div>
    <div class="col-lg-5 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Details</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Email</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control email-input" disabled="disabled">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control name-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Room</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control room-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Start Date</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control sd-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">End Date</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control ed-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Start Time</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control st-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">End Time</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control et-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">No. of Attendees</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control attendees-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Type of Event</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control event-input" disabled="disabled">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Total Cost</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon">R</span>
                                <input type="text" class="form-control cost-input" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">Total Hours</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control hours-input" disabled="disabled">
                        </div>
                    </div>


                    <div class = "hidden">

                    </div>

                    <div class="buttons">
                        <button type="button" id="approve-btn" class="btn btn-primary btn-success disabled">Approve booking</button>
                        <button type="button" id="reject-btn" class="btn btn-primary btn-danger disabled">Reject booking</button>

                        <button type ="button" id="upload-but" name="upload" data-toggle="modal" data-target="#uploadChecklistModal" disabled=true class="btn btn-primary btn-information disabled">Upload Completed Checklist</button>
                    </div>                            
                </form>

            </div>
        </div>
    </div>
    <div class="col-lg-1"></div>
    <div class="modal fade" id="uploadChecklistModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Upload Checklist</h4>
                </div>
                <div class="modal-body">
                    <form id="uploadChecklistForm" method="POST" action="./../php/upload_checklists.php" class="form-horizontal" role="form" accept-charset="UTF-8" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="checklist" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Event Checklist:</label>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                <input class="form-control" id="checklist" name="checklist" type="file" required>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                        <button class="btn btn-success submit-btn" type="submit">Upload Form</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
</div>

</body>
</html>