<?php
    require_once("./../php/config.php");
    $userList = array();
    
    session_start();
    if (!isset($_SESSION['userid']) ) {
        header("Location: ../html/login.php");
    }
    if (!isset($_SESSION['userRole']) ) {
        header("Location: ../html/index.php");
    } else {
        if (!($_SESSION['userRole'] == "Director")) {
            header("Location: ../html/index.php");
        }
    }

    $signupSuccess = 0;
    try {
        $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
        $sql = "SELECT * FROM UserDetailsTable,Login WHERE UserDetailsTable.ID = Login.userID AND Login.role = 3";
        $result = $pdo->query($sql);               
        while($row = $result->fetch()) {
            array_push($userList, $row['Name']);
        }
        $pdo = NULL;
    } catch (PDOException $e) {
        die($e->getMessage());
    }

    if (isset($_POST["make-a-user"])) {
        if(!empty($_POST["user-list"])) {
            try {
                $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
                $sql = "SELECT * FROM UserDetailsTable WHERE Name=:name";
                $prep = $pdo->prepare($sql);
                $prep->bindValue(":name", $_POST["user-list"]);
                $prep->execute();
                $row = $prep->fetch(); 
                $id = $row['ID']; 

                // set the PDO error mode to exception
                $sql = "UPDATE Login SET Role=2 WHERE userID=$id";

                // Prepare statement
                $stmt = $pdo->prepare($sql);

                // execute the query
                $stmt->execute();

                header("Location: ../html/Create_new_admin.php");
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Museum Website</title>

	<link rel="stylesheet" href="./../css/bootstrap.min.css">
	<link rel="stylesheet" href="./../css/new-admin.css">
    <link rel="stylesheet" href="./../css/general.css">
    
    <script src="./../js/jquery-1.12.2.js"></script>
    <script src="./../js/bootstrap.min.js"></script>
    <script src="./../js/new_admin.js"></script>
</head>

<body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="navbar-header" id="nav-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                <span class="sr-only">Navigation Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img class="logo-img" src="./../images/logo.jpg"/>
            <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-button">  
            <ul class="nav navbar-nav">
               <li><a id="login-btn" href="../php/logout.php">Log Out</a></li>
            </ul>
            <span class=pull-left>
                <ul class="nav navbar-nav">
                        <li>
                            <a href="Admin.php"><i class="glyphicon glyphicon-home"></i> Admin Portal</a>
                        </li>
                        <?php
                            if($_SESSION['userRole'] == "Director") {
                            echo '<li  class="active">';
                                echo '<a href="Create_new_admin.php"><i class="fa fa-fw fa-dashboard"></i> Create new admin</a>';
                            echo '</li>';
                            }
                        ?>
                        <li>
                            <a href="manage_bookings.php"><i class="fa fa-fw fa-dashboard"></i> Manage Bookings</a>
                        </li>
                        <li>
                            <a href="rooms.php"><i class="fa fa-fw fa-dashboard"></i> Manage Rooms</a>
                        </li>
                        <li>
                            <a href="reports.php"><i class="fa fa-fw fa-dashboard"></i> View Reports</a>
                        </li>
                    </ul>
            </span>
        </div>
    </nav>
    <div class = "page-header">
        <h1>
            Create new admin
        </h1>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="panel panel-success stats-pnl">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        New admin
                    </h3>
                </div>
                <form id="create" method="post">
                    <div class="panel-body text-center" id="new_admin_panel">
                        <h4>
                           Choose from the list below a user to make an administrator 
                       </h4>
                       <br><br><br>
                       <div class=col-sm-8></div>
                       <div class = col-sm-4>
                        <div class="form-group" id="date-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Users
                                </div>

                                <div class="dropdown">
                                    <input type=text class="form-control" name="user-list" placeholder="Seach users" data-toggle=dropdown id="search">
                                    <ul class="dropdown-menu" id="user-list">
                                        <?php
                                        $i = 0;
                                        foreach($userList as $user) {                                           
                                            echo "<li><a id=$i onclick=run($i)>$user</a></li>"; 
                                            $i++;                                                       
                                        }               
                                        ?>
                                    </ul>
                                    <button class="btn btn-default" name="search" id="search-but">
                                        <img class="search-img" src="./../images/search.png" width=30px height=30px/>
                                    </button> 
                                </div>
                                

                            </div>
                        </div>
                    </div>
                    <br><br><br><br><br><br>
                </div>

                <div class="panel-footer clearfix">                        
                    <span class="pull-right">
                        <button type = "submit" class="btn btn-success" name="make-a-user">
                            Make Admin
                        </button>
                    </span>
                </div>
            </form>
        </div>                
    </div>

</div>

<div class="row">
    <div class="col-lg-6">

    </div>
</div>

</div>
</body>
</html>