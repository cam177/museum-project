﻿<?php
    //include './../php/userDB.php';
    require_once("./../php/config.php");
     if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}
    if (!isset($_SESSION['userid']) ) {
        header("Location: ../html/login.php");
    }
    if (!isset($_SESSION['userRole']) ) {
        header("Location: ../html/index.php");
    } else {
        if (!($_SESSION['userRole'] == "User")) {
            header("Location: ../html/index.php");
        }
    }

function getCalendarRequest() {
    try {
        $pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
        $sql = "SELECT Rooms.roomName, Bookings.ID FROM Bookings INNER JOIN Rooms ON Bookings.roomID = Rooms.ID WHERE Bookings.userID = :user";
        $prep = $pdo->prepare($sql);
        $prep->bindValue(":user",$_SESSION['userid']);            
        $prep->execute();
        $i=0;
        while ($row = $prep->fetch(PDO::FETCH_ASSOC)) {
            echo '<li><a onclick=run('.$row['ID'].')>'.$row['roomName'].'</a></li>';
        }

    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

$pdo = new PDO(DBCONNSTRING, DBUSER, DBPASS);
$events = [];
$sql = "SELECT Rooms.roomName, Bookings.* FROM Bookings INNER JOIN Rooms WHERE Rooms.ID = Bookings.roomID";
$result = $pdo->query($sql);
while($eventInfo = $result->fetch()){
    $sql = "SELECT Email, Name, Surname FROM UserDetailsTable WHERE ID = ". $eventInfo["ID"];
    $emails = $pdo->query($sql);
    $email = $emails->fetch();
    $eventInfo["email"] = $email["Email"];
    $eventInfo["name"] = $email["Name"];
    $eventInfo["surname"] = $email["Surname"];
    $events[] = $eventInfo;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Museum Website</title>

    <link rel="stylesheet" href="./../css/user_dashboard.css">
    <link rel="stylesheet" href="./../css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link href='./../css/fullcalendar.css' rel='stylesheet' />
    <link href='./../css/fullcalendar.css' rel='stylesheet' />
    <link href='./../css/fullcalendar.print.css' rel='stylesheet' media='print' />

    <script type="text/javascript" src='./../lib/moment.min.js'></script>
    <script type="text/javascript" src='./../lib/jquery.min.js'></script>
    <script type="text/javascript" src='./../js/fullcalendar.min.js'></script>
    <script type="text/javascript"> <?php echo "var userIDs = ".$_SESSION['userid'] ?> </script>
    <script type="text/javascript" src='./../js/user_dashboard.js'></script>

    <script type="text/javascript"> <?php   echo  "var events = ". json_encode($events). ";"; ?> </script>
</head>

<body>
    
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li><a id="register-btn" href="./../php/logout.php">Log Out</a></li>
                </ul>
                <span class=pull-left>
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="user_dashboard.php"><i class="fa fa-fw fa-dashboard"></i>User Portal</a>

                        </li>
                        <li>
                            <a href="new_booking.php"><i class="fa fa-fw fa-dashboard"></i>New Booking</a>
                        </li>
                    </ul>
                </span>
            </div>
        </nav>

        <?php

        if (isset($_SESSION['msg'])) {
            	$conf = (explode("/",$_SESSION['msg']));                    
            	echo '<div class="row">';
            	echo '<div class="col-lg-1"></div>';
           	echo '<div class="col-lg-10 col-md-12">';
            	echo '<div class="alert alert-'.$conf[0].' text-center">';
           	echo '<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>';
          	echo $conf[1];
           	echo '</div>';
          	echo '</div>';
          	echo '</div>';
          	unset($_SESSION['msg']);
        }
        ?>
        
        <div class="row">
            
            
            <div class="col-md-2"></div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <?php
                    if (isset($_GET['sz'])) {
                        if ($_GET['sz'] == 'no') {
                            echo '<h3 class="error-message">Filesize too large for proof of payment</h3>';
                            unset($_GET['pw']);
                        } else if ($_GET['sz'] == 'yes') {
                            echo '<h3 class="success-message">Proof of Payment Successfuly uploaded</h3>';
                        }
                    }
                ?>
                <div class="panel panel-default stats-pnl">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">
                            Bookings
                        </h3>
                        <button class="btn btn-default pull-right"> <a href="new_booking.php"> New Booking </a></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <form action="view_booking.php" method="post">
                        <ul class="nav nav-pills nav-stacked">
                            <?php
                                getCalendarRequest();
                            ?>
                        </ul>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="panel panel-default stats-pnl">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Calendar
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
        
    </div>



</body>
</html>
