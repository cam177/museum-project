<!DOCTYPE html>
<?php
    session_start();
    if (isset($_SESSION['userid']) && isset($_SESSION['userRole'])) {
       $logInOrOut = "./../php/logout.php";
       $logStatus = "Log Out";
       $loggedIn = true;
    } else {
       $logInOrOut = "login.php";
       $logStatus = "Log In";
       $loggedIn = false;
    }

    $link = mysqli_connect('localhost', 'root', '');
    if (!$link) {
       $output = 'Unable to connect to the database server.';
       include '../html/ConnectionFail.htm';
       exit();
    }

    if (!mysqli_set_charset($link, 'utf8')){
       $output = 'Unable to set database connection encoding.';
       include '../html/ConnectionFail.htm';
       exit();
    }

    if (!mysqli_select_db($link, 'Museum')){
       $output = 'Unable to locate the database.';
       include '../html/ConnectionFail.htm';
       exit();
    }

    $result = mysqli_query($link, "SELECT ID, roomName, capacity, tariffPerHour, tariffPerDay, isDisabled, imageName FROM Rooms");
    $i = 0;
    $rooms = array();
    $rownum = mysqli_num_rows($result);
    while ($i < $rownum){
       mysqli_data_seek($result, $i);
       $row = mysqli_fetch_row($result);

       array_push($rooms, ["ID" => $row[0], "RoomName" => $row[1], "Capacity" => $row[2], "TariffPerHour" => $row[3], "TariffPerDay" => $row[4], "IsDisabled" => $row[5], "ImageName" => $row[6]]);
       $i++;
    }
?>
<html>
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Stellenbsoch University Museum</title>
   <link rel="stylesheet" href="./../css/bootstrap.min.css">
   <link rel="stylesheet" href="./../css/general.css">
   <link rel="stylesheet" href="./../css/home.css">
</head>

<body>
    <header></header>
    <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="navbar-header" id="nav-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                <span class="sr-only">Navigation Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img class="logo-img" src="./../images/logo.jpg"/>
            <a class="navbar-brand" id="nav-title" href="index.php">Stellenbsoch University Museum</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-button">  
            <ul class="nav navbar-nav">
                <?php
                    if (!$loggedIn) {
                        echo '<li class="middle-class"><a id="register-btn" href="registration.php">Register</a></li>';
                    }
                ?> 
                <li class="middle-class"><a id="login-btn" href=<?php echo $logInOrOut; ?>><?php echo $logStatus; ?></a></li>
            </ul>
        </div>
    </nav>
    <div class="page-header">
        <h1>Home Page</h1>
    </div>
    <div class="row info-row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="panel panel-default half-side-panel">
                <h3 class="panel-header">Welcome to the Museum</h3>
                <div class="panel-body">
                <p>A new exhibition on the history of Stellenbosch University (SU) will open at the University Museum, 52 Ryneveld Street, on 18 February 2015 at 18:00. The guest speaker at the official opening will be Prof Leopoldt van Huyssteen, SU's Acting Rector and Vice-Chancellor.

                Staff members, students, alumni and members of the public are welcome to attend the opening. Please confirm your attendance to ensure you get a seat. See details below.

                 
                The exhibition, titled Universiteit Stellenbosch University: verlede hede toekoms / past present future, provides a concise overview of the history of the University since the inception of Stellenbosch Gymnasium in 1866 and Stellenbosch College in 1879, which became Victoria College in 1887. SU developed from Victoria College and was opened officially on 2 April 1918. The exhibition covers the period up to 2014.
                "The history of the university buildings receives a considerable amount of attention, with a series of highlights from each of the ten decades since 1918 concerning events and developments at SU," says Prof Matilda Burden of the SU Museum, who researched, planned and installed the exhibition.

                "Over and above those highlights, the exhibition covers subjects such as the current residences, student culture, student language, sports clubs and the current faculties and departments. In addition, there is a virtual tour of the campus and a map displaying the layout of the campus, elucidated with coloured lights."

                A few interesting artefacts from academic departments that were used in teaching in years gone by are on exhibition, as well as a number of objects from three of the oldest residences: Harmonie, Dagbreek and Monica.
                </p>
                </div>
            </div>
            <div class="panel panel-default half-side-panel">
                <h3 class="panel-header">Contact Us</h3>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 header-col">
                            <h4>Museum Hours</h4>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 data-col">
                            <p>09h00 - 16h30</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 header-col">
                            <h4>Contact Number</h4>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 data-col">
                            <p>021 808 3695</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 header-col">
                            <h4>Email</h4>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 data-col">
                            <p>stellenboschuniversitymuseum@gmail.com</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 header-col">
                            <h4>Address:</h4>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 data-col">
                            <p>52 Ryneveld St</p><p>Stellenbosch</p><p>7600</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 info-col">
            <div class="panel panel-default full-side-panel">
                <h3 class="panel-header">General Information</h3>
                <div class="panel-body row">
                    <?php
                        foreach($rooms as $room) {
                           echo '<div class="row col-xs-6">';
                           echo '<figure class="image-block">';
                           echo '<img class="col-xs-12 room-img" src="./../images/' . $room['ImageName'] .'">';
                           echo '<figcaption><a>' . $room['RoomName'] .'</a>  (' . $room['Capacity'] . ' - R' . $room['TariffPerHour'] . '/hour)</figcaption>';
                           echo '</figure>';
                           echo '</div>';              
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>