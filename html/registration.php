<!DOCTYPE html>
<html>
    <head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>Stellenbsoch University Museum</title>
    	<link rel="stylesheet" href="./../css/bootstrap.min.css">
        <link rel="stylesheet" href="./../css/general.css">
        <link rel="stylesheet" href="./../css/registration.css">
    </head>
    <body>
        <header></header>
        <nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li class="middle-class"><a id="register-btn" href="../html/registration.php">Register</a></li>
                    <li class="middle-class"><a id="login-btn" href="../html/login.php">Log In</a></li>
                </ul>
            </div>
        </nav>
        <div class="page-header">
            <h1>Registration Page</h1>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <div class="panel panel-default half-side-panel">
                    <h3 class="panel-header">Registration Details</h3>
                    <div class="panel-body">
                        <form id="registration-form" method="POST" action="../php/new_registration.php" class="form-horizontal" role="form" accept-charset="UTF-8">
                            <?php
                                if (isset($outputReset)) {
                                    echo '<div class="form-group"><div class="col-xs-3"></div><div class="col-xs-7"><h4 class="no-email-message">' . $outputReset . '</h4></div></div>';
                                } elseif (isset($output)) {
                                    echo '<div class="form-group"><div class="col-xs-3"></div><div class="col-xs-7"><h5 class="error-message">' . $output . '</h5></div></div>';
                                }
                            ?>
                            <div class="form-group">
                                <label for="name" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Name:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="name" name="name" type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="surname" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Surname:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="surname" name="surname" type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="organization" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Organization Name/<br>Department Name:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="organization" name="organization" type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Address:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <textarea class="form-control" id="address" name="address" type="text" rows="4" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone_number" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Phone Number:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="phone_number" name="phone_number" type="text" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="email" name="email" type="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Password:</label>
                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                    <input class="form-control" id="password" name="password" type="password" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5"></div>
                            <button class="btn btn-success submit-btn" type="submit">Register</button>
                        </form>
                    </div>
                </div>                
            </div>
        </div>
    </body>
</html>