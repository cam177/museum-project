<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Museum Website</title>

    <link rel="stylesheet" href="./../css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link rel="stylesheet" href="./../css/bootstrap-datepicker3.css"/>

    <script type="text/javascript" src="./../js/jquery-1.12.2.js"></script>
    <script type="text/javascript" src="./../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./../js/bootstrap-datepicker.min.js"></script>
</head>

<body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="navbar-header" id="nav-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                <span class="sr-only">Navigation Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img class="logo-img" src="./../images/logo.jpg"/>
            <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-button">  
            <ul class="nav navbar-nav">
                <li><a id="register-btn" href="./../php/logout.php">Log Out</a></li>
            </ul>
            <span class=pull-left>
                <ul class="nav navbar-nav">
                    <li>
                        <a href="user_dashboard.php"><i class="fa fa-fw fa-dashboard"></i>User Portal</a>
                    </li>
                    <li class="active">
                        <a href="new_booking.php"><i class="fa fa-fw fa-dashboard"></i>New Booking</a>
                    </li>
                </ul>
            </span>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <?php
                if(isset($_GET['status']))
                {
                    echo '<div class="alert alert-'.$_GET['status'].'">';
                    echo $_GET['statement'];
                    echo '</div>';
                    
                }
            ?>
        </div>

        <div class="col-md-2"></div>
</div>

</div>

</body>
</html>
