<?php
    session_start();
    if (!isset($_SESSION['userid']) ) {
        die("HELLO1");
        header("Location: ../html/login.php");
    }
    if (!isset($_SESSION['userRole']) ) {
        header("Location: ../html/index.php");
    } else {
        if (!($_SESSION['userRole'] == "Admin" || $_SESSION['userRole'] == "Director")) {
            header("Location: ../html/index.php");
        }
    }
?>
<!DOCTYPE html>


<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Museum Website</title>
	<link rel="stylesheet" href="./../css/bootstrap.min.css">
	<link rel="stylesheet" href="./../css/general.css">
    <script src="./../js/jquery-1.12.2.js"></script>
	<script src="./../js/bootstrap.min.js"></script>
</head>
    
<body>
    <div id="wrapper">
		<nav class="navbar navbar-default navbar-custom" role="navigation">
            <div class="navbar-header" id="nav-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                    <span class="sr-only">Navigation Toggle</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img class="logo-img" src="./../images/logo.jpg"/>
                <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse-button">  
                <ul class="nav navbar-nav">
                    <li><a id="login-btn" href="../php/logout.php">Log Out</a></li>
                </ul>
                <span class=pull-left>
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="Admin.php"><i class="glyphicon glyphicon-home"></i> Admin Portal</a>
                        </li>
                        <?php
                            if($_SESSION['userRole'] == "Director") {
                            echo '<li >';
                                echo '<a href="Create_new_admin.php"><i class="fa fa-fw fa-dashboard"></i> Create new admin</a>';
                            echo '</li>';
                            }
                        ?>
                        <li>
                            <a href="manage_bookings.php"><i class="fa fa-fw fa-dashboard"></i> Manage Bookings</a>
                        </li>
                        <li>
                            <a href="rooms.php"><i class="fa fa-fw fa-dashboard"></i> Manage Rooms</a>
                        </li>
                        <li>
                            <a href="reports.php"><i class="fa fa-fw fa-dashboard"></i> View Reports</a>
                        </li>
                    </ul>
                </span>
			</div>
        </nav>
        <div class = "page-header">
            <h1>
                Admin Portal
            </h1>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div class="panel panel-success stats-pnl">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Welcome
                        </h3>
                    </div> 
                    <div class="panel-body">
                        <div class=col-sm-2></div>
                        <div class=col-sm-8>
                            <img class="img-responsive center-block" src="./../images/museum-front.jpg" width=100% height=85%>
                        </div>
                    </div>
                </div>            
            </div>
        </div>
            
        <div class="row">
            <div class="col-lg-6">
                
            </div>
        </div>
        
    </div>
</body>
</html>