<?php

session_start();
if (!isset($_SESSION['userid']) ) {
    header("Location: ../html/login.php");
}
if (!isset($_SESSION['userRole']) ) {
    header("Location: ../html/index.php");
} else {
    if (!($_SESSION['userRole'] == "User")) {
        header("Location: ../html/index.php");
    }
}

include './../php/bookingDB.php';
include './../php/room_info.php';

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Museum Website</title>

    <link rel="stylesheet" href="./../css/new_booking.css">
    <link rel="stylesheet" href="./../css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/general.css">
    <link rel="stylesheet" href="./../css/bootstrap-datepicker3.css"/>

    <script type="text/javascript" src="./../js/jquery-1.12.2.js"></script>
    <script type="text/javascript" src="./../js/new_booking.js"></script>
    <script type="text/javascript" src="./../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./../js/bootstrap-datepicker.min.js"></script>
</head>

<body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="navbar-header" id="nav-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" id="nav-button-bars">
                <span class="sr-only">Navigation Toggle</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img class="logo-img" src="./../images/logo.jpg"/>
            <a class="navbar-brand" id="nav-title" href="../html/index.php">Stellenbsoch University Museum</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-button">  
            <ul class="nav navbar-nav">
                <li><a id="register-btn" href="./../php/logout.php">Log Out</a></li>
            </ul>
            <span class=pull-left>
                <ul class="nav navbar-nav">
                    <li>
                        <a href="user_dashboard.php"><i class="fa fa-fw fa-dashboard"></i>User Portal</a>
                    </li>
                    <li class="active">
                        <a href="new_booking.php"><i class="fa fa-fw fa-dashboard"></i>New Booking</a>
                    </li>
                </ul>
            </span>
        </div>
    </nav>

    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">
                        Create New Booking
                    </h3>
                    <button class="btn btn-default pull-right" id="plus">Add Room</button>
                    <div class="clearfix"></div>
                </div>
                
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="./../php/place_booking.php">
                        <div class="panel panel-default room_panel" id="mainRoom">
                            <div class="panel-body">
                              
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Room</label>
                                    <div class="col-sm-1">
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu rooms">
                                                <?php
                                                findRooms();
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control roomName" name="roomName" disabled="disabled" placeholder="Name">
                                        <input type="text" class="form-control roomID collapse" name="roomID">
                                        <div class="alert alert-danger alert_rooms collapse">No room <strong> selected</strong></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Capacity</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control capacity" name="capacity" disabled="disabled" placeholder="0">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Tariff Per Hour</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control tariff_per_hour" name="tariffPerHour" disabled="disabled" placeholder="0">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Tariff Per Day</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control tariff_per_day" name="tariffPerDay" disabled="disabled" placeholder="0">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Minimum Hire Time</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control minimum_hire_time" name="minimumHireTime" disabled="disabled" placeholder="0 Hours">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Available From</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control avail_from" disabled="disabled" placeholder="00:00:00">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Available To</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control avail_to" disabled="disabled" placeholder="00:00:00">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Start Date</label>
                            <div class="col-sm-10">
                                <input class="form-control date start_date" name="avaliableFrom" placeholder="YYYY-MM-DD" type="text"/>
                                <div class="alert alert-danger alert_start_date collapse"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">End Date</label>
                            <div class="col-sm-10">
                                <input class="form-control date end_date" name="avaliableTo" placeholder="YYYY-MM-DD" type="text"/>
                                <div class="alert alert-danger alert_end_date collapse"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2">Start Time</label>
                            <div class="col-sm-10">
                             <div class="input-group">
                                <input type="text" class="form-control input-small start_time" name="startTime" placeholder="00:00">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                            <div class="alert alert-danger alert_start_time collapse">Needs to be in format <strong>HH:MM</strong></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2">End Time</label>
                        <div class="col-sm-10">
                         <div class="input-group">
                            <input type="text" class="form-control input-small end_time" name="endTime" placeholder="00:00">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                        </div>
                        <div class="alert alert-danger alert_end_time collapse">Needs to be in format <strong>HH:MM</strong></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">No. of Attendees</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control form-field attendees" name="attendees" placeholder="Enter Number">
                        <div class="alert alert-danger alert_attendees alert-collapse collapse"></div>
                    </div>
                </div>
                

                <div class="form-group">
                    <label class="control-label col-sm-2">Type of Event</label>
                    <div class="col-sm-10">
                        <div class="dropdown">
                           <select name="type" class="form-control">
                                <option value="Party">Party</option>
                                <option value="Conference">Conference</option> 
                                <option value="Lecture">Lecture</option>
                                <option value="Other">Other</option>                                  
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Total Cost</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon">R</span>
                            <input type="text" class="form-control total_cost" disabled="disabled" placeholder="0">
                        </div>
                        <input type="text" class="form-control total_costH collapse" name="total_cost">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Total Hours</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control total_hours" disabled="disabled" placeholder="0">
                        <input type="text" class="form-control total_hoursH collapse" name="total_hours">
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="alert alert-danger alert_submit alert-collapse collapse">Please fix errors above</div>
                    </div>
                </div>

                <input type="text" class="form-control roomIDs collapse" name="roomIDs">
                <input type="text" class="form-control valid collapse" name="valid">

                <div class="form-group">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary submit btn-success" name="submit">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div class="col-md-2"></div>
</div>

</div>

<script>
    $(document).ready(function(){
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        $('.date').datepicker({
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>

</body>
</html>
