-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 20, 2016 at 03:00 AM
-- Server version: 10.1.11-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Museum`
--

-- --------------------------------------------------------

--
-- Table structure for table `Bookings`
--

CREATE TABLE `Bookings` (
  `ID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `roomID` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `startTime` time NOT NULL,
  `endTime` time NOT NULL,
  `attendees` int(4) NOT NULL,
  `eventType` varchar(255) NOT NULL,
  `cost` decimal(65,0) NOT NULL,
  `totalHours` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `uploadedChecklist` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Bookings`
--

INSERT INTO `Bookings` (`ID`, `userID`, `roomID`, `startDate`, `endDate`, `startTime`, `endTime`, `attendees`, `eventType`, `cost`, `totalHours`, `approved`, `uploadedChecklist`) VALUES
(1, 3, 1, '2016-10-26', '2016-10-27', '02:08:21', '04:22:25', 20, 'conference', '200', 2, 0, NULL),
(2, 3, 3, '2016-10-26', '2016-10-27', '10:00:00', '12:00:00', 20, 'conference', '200', 2, 0, NULL),
(3, 4, 2, '2016-10-26', '2016-10-30', '14:00:00', '17:00:00', 10, 'Conference', '50', 3, 2, NULL),
(4, 4, 1, '2016-10-13', '2016-10-14', '11:00:00', '17:00:00', 20, 'Party', '0', 12, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ForgottenPasswordRequests`
--

CREATE TABLE `ForgottenPasswordRequests` (
  `userID` int(11) NOT NULL,
  `resetToken` varchar(255) NOT NULL,
  `ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Login`
--

CREATE TABLE `Login` (
  `userID` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `role` int(1) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Login`
--

INSERT INTO `Login` (`userID`, `Email`, `Password`, `role`, `Active`) VALUES
(1, 'pedrie95@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 1),
(2, 'brian.nvidia@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 1),
(3, 'dylanpivo@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 3, 1),
(4, 'cameronraven56@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `MuseumDetails`
--

CREATE TABLE `MuseumDetails` (
  `ID` int(11) NOT NULL,
  `bankAccountNumber` varchar(255) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `accountHolder` varchar(255) NOT NULL,
  `costPointAccountNumber` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MuseumDetails`
--

INSERT INTO `MuseumDetails` (`ID`, `bankAccountNumber`, `bank`, `branch`, `accountHolder`, `costPointAccountNumber`, `reference`) VALUES
(1, '073006955', 'Standard Bank', 'Stellenbosch', 'US Deposito Rekening', '2162b', 'Museum');

-- --------------------------------------------------------

--
-- Table structure for table `Roles`
--

CREATE TABLE `Roles` (
  `NameOfRole` varchar(10) NOT NULL,
  `Role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Roles`
--

INSERT INTO `Roles` (`NameOfRole`, `Role`) VALUES
('Director', 1),
('Admin', 2),
('User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Rooms`
--

CREATE TABLE `Rooms` (
  `ID` int(11) NOT NULL,
  `roomName` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL,
  `tariffPerHour` double NOT NULL,
  `tariffPerDay` double NOT NULL,
  `isDisabled` tinyint(1) NOT NULL,
  `minimumHireTime` int(11) NOT NULL,
  `availableFrom` time NOT NULL,
  `availableTo` time NOT NULL,
  `imageName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Rooms`
--

INSERT INTO `Rooms` (`ID`, `roomName`, `capacity`, `tariffPerHour`, `tariffPerDay`, `isDisabled`, `minimumHireTime`, `availableFrom`, `availableTo`, `imageName`) VALUES
(1, 'Lecture Theatre', 40, 600, 4800, 0, 0, '11:00:00', '21:00:00', 'lectureTheater.jpg'),
(2, 'Front Foyer', 200, 1250, 5000, 0, 0, '17:00:00', '21:00:00', 'frontfoyer.jpg'),
(3, 'Mike de Vries Hall', 200, 700, 5600, 0, 0, '11:00:00', '21:00:00', 'mikeDeVries1.jpg'),
(4, 'Museum Courtyard', 300, 600, 4800, 0, 0, '11:00:00', '21:00:00', 'courtyard1.jpg'),
(5, 'Back Foyer', 100, 200, 1600, 0, 2, '11:00:00', '21:00:00', 'backfoyer1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `UserDetailsTable`
--

CREATE TABLE `UserDetailsTable` (
  `ID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Surname` varchar(255) NOT NULL,
  `organisationName` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `phoneNumber` varchar(10) NOT NULL,
  `Email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserDetailsTable`
--

INSERT INTO `UserDetailsTable` (`ID`, `Name`, `Surname`, `organisationName`, `Address`, `phoneNumber`, `Email`) VALUES
(1, 'Pedrie', 'Oosthuizen', ' ', ' ', '1234567890', 'pedrie95@gmail.com'),
(2, 'Brian', 'Schirmacher', ' ', ' ', '0123456789', 'brian.nvidia@gmail.com'),
(3, 'Dylan', 'Pivo ', ' ', ' ', '2345678911', 'dylanpivo@gmail.com'),
(4, 'cameron', 'raven', ' ', 'Eastcliff', '838608850', 'cameronraven56@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Bookings`
--
ALTER TABLE `Bookings`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `roomID` (`roomID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `ForgottenPasswordRequests`
--
ALTER TABLE `ForgottenPasswordRequests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `Login`
--
ALTER TABLE `Login`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `role` (`role`),
  ADD KEY `userID` (`userID`);

--
-- Indexes for table `MuseumDetails`
--
ALTER TABLE `MuseumDetails`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `bank` (`bank`),
  ADD UNIQUE KEY `branch` (`branch`),
  ADD UNIQUE KEY `accountHolder` (`accountHolder`),
  ADD UNIQUE KEY `costPointAccountNumber` (`costPointAccountNumber`),
  ADD UNIQUE KEY `reference` (`reference`);

--
-- Indexes for table `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`Role`);

--
-- Indexes for table `Rooms`
--
ALTER TABLE `Rooms`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `UserDetailsTable`
--
ALTER TABLE `UserDetailsTable`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Email` (`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Bookings`
--
ALTER TABLE `Bookings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ForgottenPasswordRequests`
--
ALTER TABLE `ForgottenPasswordRequests`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `MuseumDetails`
--
ALTER TABLE `MuseumDetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Rooms`
--
ALTER TABLE `Rooms`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `UserDetailsTable`
--
ALTER TABLE `UserDetailsTable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Bookings`
--
ALTER TABLE `Bookings`
  ADD CONSTRAINT `Bookings_ibfk_1` FOREIGN KEY (`roomID`) REFERENCES `Rooms` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Bookings_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `UserDetailsTable` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ForgottenPasswordRequests`
--
ALTER TABLE `ForgottenPasswordRequests`
  ADD CONSTRAINT `ForgottenPasswordRequests_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `UserDetailsTable` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Login`
--
ALTER TABLE `Login`
  ADD CONSTRAINT `Login_ibfk_1` FOREIGN KEY (`role`) REFERENCES `Roles` (`Role`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Login_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `UserDetailsTable` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
